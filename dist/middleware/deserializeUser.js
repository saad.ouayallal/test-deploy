"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const jwt_1 = require("../db/jwt");
const lodash_1 = require("lodash");
const auth_service_1 = require("../service/auth.service");
const deserializeUser = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    const accessToken = (0, lodash_1.get)(req, "headers.authorization", "").replace(/^Bearer\s/, "");
    const refreshToken = (0, lodash_1.get)(req, "headers.x-refresh");
    if (!accessToken || accessToken === "null" || !refreshToken || refreshToken === "null") {
        // Handle appropriately
        return next();
    }
    const { decoded, expired } = (0, jwt_1.verifyJwt)(accessToken, "accessTokenPublicKey");
    if (decoded) {
        res.locals.user = decoded;
        console.log("heeeeere");
        return next();
    }
    if (expired && refreshToken) {
        const newAccessToken = yield (0, auth_service_1.reIssueAccessToken)({ refreshToken: refreshToken });
        if (newAccessToken) {
            res.setHeader("x-access-token", newAccessToken);
        }
        const result = (0, jwt_1.verifyJwt)(newAccessToken, "accessTokenPublicKey");
        res.locals.user = result.decoded;
        return next();
    }
    return next();
});
exports.default = deserializeUser;
