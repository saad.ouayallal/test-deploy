"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typegoose_1 = require("@typegoose/typegoose");
const index_1 = require("./index");
let ProductInfo = class ProductInfo {
};
__decorate([
    (0, typegoose_1.prop)({ ref: "Audio" }),
    __metadata("design:type", Object)
], ProductInfo.prototype, "product", void 0);
ProductInfo = __decorate([
    (0, typegoose_1.index)({ email: 1 }),
    (0, typegoose_1.modelOptions)({
        schemaOptions: {
            timestamps: true, // to add timestamps like createdAt & updatedAt
        },
        options: {
            allowMixed: typegoose_1.Severity.ALLOW,
        },
    })
], ProductInfo);
class Order {
}
__decorate([
    (0, typegoose_1.prop)({ type: () => [ProductInfo] }),
    __metadata("design:type", Array)
], Order.prototype, "products", void 0);
__decorate([
    (0, typegoose_1.prop)(),
    __metadata("design:type", Object)
], Order.prototype, "paymentIntent", void 0);
__decorate([
    (0, typegoose_1.prop)({
        enum: [
            "Not Processed",
            "Payment Online",
            "Processing",
            "Dispatched",
            "Cancelled",
            "Delivered",
        ],
        default: "Not Processed",
    }),
    __metadata("design:type", String)
], Order.prototype, "orderStatus", void 0);
__decorate([
    (0, typegoose_1.prop)({ ref: () => index_1.User }),
    __metadata("design:type", Object)
], Order.prototype, "orderby", void 0);
const OrderModel = (0, typegoose_1.getModelForClass)(Order);
exports.default = OrderModel;
