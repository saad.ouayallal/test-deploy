"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getUserSessionsHandler = exports.signOutHandler = exports.createAdminSessionHandler = exports.createUserSessionHandler = void 0;
const auth_service_1 = require("../service/auth.service");
const user_service_1 = require("../service/user.service");
const jwt_1 = require("../db/jwt");
const config_1 = __importDefault(require("config"));
function createUserSessionHandler(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const message = "Invalid email or password";
            const user = yield (0, user_service_1.validatePassword)(req.body);
            if (!user) {
                return res.status(400).send({ errorMessage: message });
            }
            // Create a session
            const session = yield (0, auth_service_1.createSession)(user._id.toString(), req.get("user-agent") || "");
            // create an access token
            const accessToken = (0, jwt_1.signJwt)(Object.assign(Object.assign({}, user), { session: session._id }), "accessTokenPrivateKey", { expiresIn: config_1.default.get("accessTokenTtl") } // 15 minutes,
            );
            // create a refresh token
            const refreshToken = (0, jwt_1.signJwt)(Object.assign(Object.assign({}, user), { session: session._id }), "refreshTokenPrivateKey", { expiresIn: config_1.default.get("refreshTokenTtl") } // 15 minutes
            );
            // Set cookies
            res.cookie("accessToken", accessToken, {
                httpOnly: true,
                secure: true,
                maxAge: 900000, // Convert expiresIn to milliseconds
            });
            res.cookie("refreshToken", refreshToken, {
                httpOnly: true,
                secure: true,
                maxAge: 900000, // Convert expiresIn to milliseconds
            });
            // return access & refresh tokens
            return res
                .status(200)
                .send({ code: 200, message: { accessToken, refreshToken } });
        }
        catch (error) {
            return res.status(500).send({
                errorMessage: "Internal error",
            });
        }
    });
}
exports.createUserSessionHandler = createUserSessionHandler;
function createAdminSessionHandler(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const message = "Invalid email or password";
            const user = yield (0, user_service_1.validatePassword)(req.body);
            if (!user) {
                return res.status(400).send({ errorMessage: message });
            }
            if (user.role !== "admin") {
                return res.status(400).send({ errorMessage: message });
            }
            // Create a session
            const session = yield (0, auth_service_1.createSession)(user._id.toString(), req.get("user-agent") || "");
            // create an access token
            const accessToken = (0, jwt_1.signJwt)(Object.assign(Object.assign({}, user), { session: session._id }), "accessTokenPrivateKey", { expiresIn: config_1.default.get("accessTokenTtl") } // 15 minutes,
            );
            // create a refresh token
            const refreshToken = (0, jwt_1.signJwt)(Object.assign(Object.assign({}, user), { session: session._id }), "refreshTokenPrivateKey", { expiresIn: config_1.default.get("refreshTokenTtl") } // 15 minutes
            );
            // Set cookies
            res.cookie("accessToken", accessToken, {
                httpOnly: true,
                secure: true,
                maxAge: 900000, // Convert expiresIn to milliseconds
            });
            res.cookie("refreshToken", refreshToken, {
                httpOnly: true,
                secure: true,
                maxAge: 900000, // Convert expiresIn to milliseconds
            });
            // return access & refresh tokens
            return res
                .status(200)
                .send({ code: 200, message: { accessToken, refreshToken } });
        }
        catch (error) {
            return res.status(500).send({
                errorMessage: "Internal error",
            });
        }
    });
}
exports.createAdminSessionHandler = createAdminSessionHandler;
function signOutHandler(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const sessionId = res.locals.user.session;
            yield (0, auth_service_1.updateSession)({ _id: sessionId }, { valid: false });
            return res.status(200).send({
                code: 200,
                message: "User Successfully logged out",
                accessToken: null,
                refreshToken: null,
            });
        }
        catch (error) {
            return res.status(500).send({
                errorMessage: "Internal error",
            });
        }
    });
}
exports.signOutHandler = signOutHandler;
function getUserSessionsHandler(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const userId = res.locals.user._id;
        const sessions = yield (0, auth_service_1.findSessions)({ user: userId, valid: true });
        return res.send(sessions);
    });
}
exports.getUserSessionsHandler = getUserSessionsHandler;
