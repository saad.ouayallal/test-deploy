"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getallCategory = exports.getCategory = exports.deleteCategory = exports.updateCategory = exports.createCategory = void 0;
const category_service_1 = require("../service/category.service");
const validateMongoDBId_1 = __importDefault(require("../utils/validateMongoDBId"));
//
function createCategory(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const newCategory = yield (0, category_service_1.createCategoryinDb)(req.body);
            res.status(200).json({ code: 200, result: newCategory });
        }
        catch (error) {
            return res.status(500).send({ errorMessage: error.message });
        }
    });
}
exports.createCategory = createCategory;
function updateCategory(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const { id } = req.params;
        (0, validateMongoDBId_1.default)(id);
        try {
            const updatedCategory = yield (0, category_service_1.findByIdAndUpdate)(id);
            res.status(200).json({ code: 200, result: updatedCategory });
        }
        catch (error) {
            return res.status(500).send({ errorMessage: error.message });
        }
    });
}
exports.updateCategory = updateCategory;
function deleteCategory(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const { id } = req.params;
        (0, validateMongoDBId_1.default)(id);
        try {
            const deletedCategory = yield (0, category_service_1.findByIdAndDelete)(id);
            if (!deleteCategory) {
                return res.status(404).json({ errorMessage: `Category not found` });
            }
            else
                return res.status(200).json({ code: 200, message: `Category ${deletedCategory === null || deletedCategory === void 0 ? void 0 : deletedCategory.title} deleted successfuly` });
        }
        catch (error) {
            return res.status(500).send({ errorMessage: error.message });
        }
    });
}
exports.deleteCategory = deleteCategory;
//
function getCategory(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const { id } = req.params;
        (0, validateMongoDBId_1.default)(id);
        try {
            const getaCategory = yield (0, category_service_1.findCategory)(id);
            res.status(200).json({ code: 200, result: getaCategory });
        }
        catch (error) {
            return res.status(500).send({ errorMessage: error.message });
        }
    });
}
exports.getCategory = getCategory;
//
function getallCategory(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const getallCategory = yield (0, category_service_1.findAllCategory)();
            res.status(200).send({ code: 200, result: getallCategory });
        }
        catch (error) {
            return res.status(500).send({ errorMessage: error.message });
        }
    });
}
exports.getallCategory = getallCategory;
