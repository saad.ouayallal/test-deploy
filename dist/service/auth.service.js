"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.reIssueAccessToken = exports.findSessions = exports.logoutSession = exports.signAccessToken = exports.updateSession = exports.deleteSessionById = exports.findSessionByUser = exports.findSessionById = exports.createSession = void 0;
const lodash_1 = require("lodash");
const session_model_1 = __importDefault(require("../models/session.model"));
const user_model_1 = require("../models/user.model");
const jwt_1 = require("../db/jwt");
const user_service_1 = require("./user.service");
const lodash_2 = require("lodash");
const config_1 = __importDefault(require("config"));
// export async function createSession({ userId }: { userId: string }) {
//   return SessionModel.create({ user: userId });
// }
function createSession(userId, userAgent) {
    return __awaiter(this, void 0, void 0, function* () {
        const session = yield session_model_1.default.create({ user: userId, userAgent });
        return session.toJSON();
    });
}
exports.createSession = createSession;
function findSessionById(id) {
    return __awaiter(this, void 0, void 0, function* () {
        return session_model_1.default.findById(id);
    });
}
exports.findSessionById = findSessionById;
function findSessionByUser(user) {
    return __awaiter(this, void 0, void 0, function* () {
        return session_model_1.default.findOne({ user });
    });
}
exports.findSessionByUser = findSessionByUser;
function deleteSessionById(id) {
    return __awaiter(this, void 0, void 0, function* () {
        return session_model_1.default.findByIdAndDelete(id);
    });
}
exports.deleteSessionById = deleteSessionById;
function updateSession(query, update) {
    return __awaiter(this, void 0, void 0, function* () {
        return session_model_1.default.updateOne(query, update);
    });
}
exports.updateSession = updateSession;
function signAccessToken(user) {
    const payload = (0, lodash_1.omit)(user.toJSON(), user_model_1.privateFields);
    const accessToken = (0, jwt_1.signJwt)(payload, "accessTokenPrivateKey", {
        expiresIn: "15m",
    });
    return accessToken;
}
exports.signAccessToken = signAccessToken;
function logoutSession(sessionId) {
    return __awaiter(this, void 0, void 0, function* () {
        const session = yield deleteSessionById(sessionId);
        return session;
    });
}
exports.logoutSession = logoutSession;
function findSessions(query) {
    return __awaiter(this, void 0, void 0, function* () {
        return session_model_1.default.find(query).lean();
    });
}
exports.findSessions = findSessions;
function reIssueAccessToken({ refreshToken, }) {
    return __awaiter(this, void 0, void 0, function* () {
        const { decoded } = (0, jwt_1.verifyJwt)(refreshToken, "refreshTokenPublicKey");
        if (!decoded || !(0, lodash_2.get)(decoded, "session"))
            return false;
        const session = yield session_model_1.default.findById((0, lodash_2.get)(decoded, "session"));
        if (!session || !session.valid)
            return false;
        const user = yield (0, user_service_1.findUser)({ _id: session.user });
        if (!user)
            return false;
        const accessToken = (0, jwt_1.signJwt)(Object.assign(Object.assign({}, user), { session: session._id }), "accessTokenPrivateKey", { expiresIn: config_1.default.get("accessTokenTtl") } // 15 minutes
        );
        return accessToken;
    });
}
exports.reIssueAccessToken = reIssueAccessToken;
