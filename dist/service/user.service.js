"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.validatePassword = exports.deleteUser = exports.findUserByEmail = exports.findUser = exports.findUsers = exports.findUserByIdAndUpdate = exports.findUserById = exports.createUser = void 0;
const lodash_1 = require("lodash");
const index_1 = require("../models/index");
function createUser(input) {
    return __awaiter(this, void 0, void 0, function* () {
        return index_1.UserModel.create(input);
    });
}
exports.createUser = createUser;
function findUserById(id) {
    return index_1.UserModel.findById(id);
}
exports.findUserById = findUserById;
function findUserByIdAndUpdate(id) {
    return index_1.UserModel.findByIdAndUpdate(id);
}
exports.findUserByIdAndUpdate = findUserByIdAndUpdate;
function findUsers() {
    return index_1.UserModel.find();
}
exports.findUsers = findUsers;
function findUser(query) {
    return index_1.UserModel.findOne(query).lean();
}
exports.findUser = findUser;
function findUserByEmail(email) {
    return __awaiter(this, void 0, void 0, function* () {
        return index_1.UserModel.findOne({ email });
    });
}
exports.findUserByEmail = findUserByEmail;
function deleteUser(id) {
    return index_1.UserModel.findByIdAndDelete(id);
}
exports.deleteUser = deleteUser;
function validatePassword({ email, password, }) {
    return __awaiter(this, void 0, void 0, function* () {
        const user = yield index_1.UserModel.findOne({ email });
        if (!user) {
            return false;
        }
        const isValid = yield user.comparePassword(password);
        if (!isValid)
            return false;
        return (0, lodash_1.omit)(user.toJSON(), "password");
    });
}
exports.validatePassword = validatePassword;
