"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.findAllCategory = exports.findCategory = exports.findByIdAndDelete = exports.findByIdAndUpdate = exports.updateCategoryinDb = exports.createCategoryinDb = void 0;
const index_1 = require("../models/index");
function createCategoryinDb(input) {
    return __awaiter(this, void 0, void 0, function* () {
        return index_1.CategoryModel.create(input);
    });
}
exports.createCategoryinDb = createCategoryinDb;
function updateCategoryinDb(input) {
    return __awaiter(this, void 0, void 0, function* () {
        // return CategoryModel.updateOne(input);
    });
}
exports.updateCategoryinDb = updateCategoryinDb;
function findByIdAndUpdate(id) {
    return index_1.CategoryModel.findByIdAndUpdate(id);
}
exports.findByIdAndUpdate = findByIdAndUpdate;
function findByIdAndDelete(id) {
    return index_1.CategoryModel.findByIdAndDelete(id);
}
exports.findByIdAndDelete = findByIdAndDelete;
function findCategory(id) {
    return index_1.CategoryModel.findById(id);
}
exports.findCategory = findCategory;
function findAllCategory() {
    return index_1.CategoryModel.find();
}
exports.findAllCategory = findAllCategory;
