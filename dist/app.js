"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
require("dotenv").config();
const express_1 = __importDefault(require("express"));
const config_1 = __importDefault(require("config"));
const connection_1 = require("./db/connection");
const logger_1 = __importDefault(require("./db/logger"));
const user_1 = __importDefault(require("./routes/user"));
const swagger_1 = __importDefault(require("./utils/swagger"));
const product_1 = __importDefault(require("./routes/product"));
const category_1 = __importDefault(require("./routes/category"));
const cookie_parser_1 = __importDefault(require("cookie-parser"));
const cors_1 = __importDefault(require("cors"));
const deserializeUser_1 = __importDefault(require("./middleware/deserializeUser"));
const app = (0, express_1.default)();
const allowedOrigins = ["http://localhost:3000", "http://localhost:4200"];
const options = {
    origin: allowedOrigins,
};
app.use((0, cors_1.default)(options));
app.use((0, cookie_parser_1.default)());
app.use(express_1.default.json());
app.use(deserializeUser_1.default);
app.use(user_1.default);
app.use(product_1.default);
app.use(category_1.default);
const port = config_1.default.get("port");
(0, connection_1.connectToServer)().then(() => __awaiter(void 0, void 0, void 0, function* () {
    app.listen(port, () => {
        logger_1.default.info(`Express is listening at http://localhost:${port}`);
        (0, swagger_1.default)(app, port);
    });
}));
