"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const auth_controller_1 = require("../../controllers/auth.controller");
const requireUser_1 = require("../../middleware/requireUser");
const validateResource_1 = __importDefault(require("../../middleware/validateResource"));
const auth_schema_1 = require("../../schema/auth.schema");
const router = express_1.default.Router();
router.post("/api/sessions", (0, validateResource_1.default)(auth_schema_1.createSessionSchema), auth_controller_1.createUserSessionHandler);
/**
 * @swagger
 * /api/sessions/admin:
 *   post:
 *     summary: Create an Admin Session
 *     tags: [Session]
 *     requestBody:
 *      required: true
 *      content:
 *        application/json:
 *           schema:
 *              $ref: '#/components/schemas/createSessionInput'
 *     responses:
 *       200:
 *         description: Successful response with access and refresh tokens
 *         content:
 *           application/json:
 *             schema:
 *              $ref: '#/components/schemas/createSessionResponse'
 *       400:
 *         description: Invalid email or password
 *         content:
 *           application/json:
 *             schema:
 *              $ref: '#/components/schemas/ErrorResponse'
 *       401:
 *         description: User is not an Admin
 *         content:
 *           application/json:
 *             schema:
 *              $ref: '#/components/schemas/ErrorResponse'
 *       500:
 *         description: Invalid email or password
 *         content:
 *           application/json:
 *            example:
 *               errorMessage: Internal error
 */
router.post("/api/sessions/admin", (0, validateResource_1.default)(auth_schema_1.createSessionSchema), auth_controller_1.createAdminSessionHandler);
/**
   * @swagger
   * '/api/sessions':
   *  get:
   *    tags:
   *    - Session
   *    summary: Get all sessions
   *    responses:
   *      200:
   *        description: Get all sessions for current user
   *        content:
   *          application/json:
   *            schema:
   *              $ref: '#/components/schemas/GetSessionResponse'
   *      403:
   *        description: Forbidden
   *  post:
   *    tags:
   *    - Session
   *    summary: Create a session
   *    requestBody:
   *      required: true
   *      content:
   *        application/json:
   *          schema:
   *            $ref: '#/components/schemas/CreateSessionInput'
   *    responses:
   *      200:
   *        description: Session created
   *        content:
   *          application/json:
   *            schema:
   *              $ref: '#/components/schemas/CreateSessionResponse'
   *      401:
   *        description: Unauthorized
   *  delete:
   *    tags:
   *    - Session
   *    summary: Delete a session
   *    responses:
   *      200:
   *        description: Session deleted
   *      403:
   *        description: Forbidden
   */
router.get("/api/sessions", requireUser_1.requireUser, auth_controller_1.getUserSessionsHandler);
router.delete("/api/sessions", requireUser_1.requireUser, auth_controller_1.signOutHandler);
exports.default = router;
