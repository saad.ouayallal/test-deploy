"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.resetPasswordSchema = exports.forgotPasswordSchema = exports.turnUsertoAdminSchema = exports.verifyUserSchema = exports.createUserSchema = void 0;
const zod_1 = require("zod");
/**
* @openapi
* components:
*  schemas:
*   CreateUserInput:
*     type: object
*     required:
*       - firstName
*       - lastName
*       - password
*       - passwordConfirmation
*       - email
*       - adresse
*       - phoneNumber
*     properties:
*       firstName:
*         type: string
*       lastName:
*         type: string
*       password:
*         type: string
*         minLength: 6
*       passwordConfirmation:
*         type: string
*         minLength: 6
*       email:
*         type: string
*         format: email
*       adresse:
*         type: string
*       phoneNumber:
*         type: string
*   CreateUserResponse:
*     type: object
*     properties:
*       code:
*         type: integer
*       message:
*         type: string
 */
exports.createUserSchema = (0, zod_1.object)({
    body: (0, zod_1.object)({
        firstName: (0, zod_1.string)({
            required_error: "First name is required",
        }),
        lastName: (0, zod_1.string)({
            required_error: "Last name is required",
        }),
        password: (0, zod_1.string)({
            required_error: "Password is required",
        }).min(6, "Password is too short - should be min 6 chars"),
        passwordConfirmation: (0, zod_1.string)({
            required_error: "Password confirmation is required",
        }),
        email: (0, zod_1.string)({
            required_error: "Email is required",
        }).email("Not a valid email"),
    }).refine((data) => data.password === data.passwordConfirmation, {
        message: "Passwords do not match",
        path: ["passwordConfirmation"],
    }),
});
/**
 * @openapi
 * components:
 *   schemas:
 *     VerifyUserParams:
 *       type: object
 *       properties:
 *         id:
 *           type: string
 *         verificationCode:
 *           type: string
 *       required:
 *         - id
 *         - verificationCode
 */
exports.verifyUserSchema = (0, zod_1.object)({
    params: (0, zod_1.object)({
        id: (0, zod_1.string)(),
        verificationCode: (0, zod_1.string)(),
    }),
});
exports.turnUsertoAdminSchema = (0, zod_1.object)({
    params: (0, zod_1.object)({
        id: (0, zod_1.string)()
    }),
});
/**
 * @swagger
 * components:
 *   schemas:
 *     ForgotPasswordInput:
 *       type: object
 *       properties:
 *         email:
 *           type: string
 *           format : email
 *       required:
 *         - email
 */
exports.forgotPasswordSchema = (0, zod_1.object)({
    body: (0, zod_1.object)({
        email: (0, zod_1.string)({
            required_error: "Email is required",
        }).email("Not a valid email"),
    }),
});
/**
 * @swagger
 * components:
 *   schemas:
 *     ResetPasswordParams:
 *       type: object
 *       properties:
 *         id:
 *           type: string
 *         passwordResetCode:
 *           type: string
 *       required:
 *         - id
 *         - passwordResetCode
 *     ResetPasswordInput:
 *       type: object
 *       properties:
 *         password:
 *           type: string
 *         passwordConfirmation:
 *           type: string
 *       required:
 *         - password
 *         - passwordConfirmation
 */
exports.resetPasswordSchema = (0, zod_1.object)({
    params: (0, zod_1.object)({
        id: (0, zod_1.string)(),
        passwordResetCode: (0, zod_1.string)(),
    }),
    body: (0, zod_1.object)({
        password: (0, zod_1.string)({
            required_error: "Password is required",
        }).min(6, "Password is too short - should be min 6 chars"),
        passwordConfirmation: (0, zod_1.string)({
            required_error: "Password confirmation is required",
        }),
    }).refine((data) => data.password === data.passwordConfirmation, {
        message: "Passwords do not match",
        path: ["passwordConfirmation"],
    }),
});
