import { object, string, array, number, TypeOf, z } from "zod";

const MAX_FILE_SIZE = 5000000;
const ACCEPTED_AUDIO_TYPES = [
  "image/jpeg",
  "image/jpg",
  "image/png",
  "image/webp",
  "audio/mpeg",
];

export const createAudioSchema = z.object({
  profileAudio: z
    .any()
    .refine((files) => files?.length == 1, "Audio is required.")
    .refine(
      (files) => files?.[0]?.size <= MAX_FILE_SIZE,
      `Max file size is 50MB.`
    )
    .refine(
      (files) => ACCEPTED_AUDIO_TYPES.includes(files?.[0]?.type),
      ".jpg, .jpeg, .png and .webp files are accepted."
    ),
  title: string({
    required_error: "title is required",
  }),
  duration: number({
    required_error: "duration is required",
  }),
  category: string({
    required_error: "Category is required",
  }),
  email: string({
    required_error: "Email is required",
  }).email("Not a valid email"),
});

export const updateAudioSchema = object({
  params: object({
    id: string(),
  }),
  body: object({
    name: string({
      required_error: "Audio name is required",
    }),
  }),
});

export const removeAudioSchema = object({
  params: object({
    id: string(),
  }),
});


export type CreateAudioInput = TypeOf<typeof createAudioSchema>;

export type UpdateAudioInput = TypeOf<typeof updateAudioSchema>["body"];

export type RemoveAudioInput = TypeOf<typeof removeAudioSchema>["params"];

export type UpdateAudioParams = TypeOf<typeof updateAudioSchema>["params"];
