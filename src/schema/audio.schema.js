"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.removeAudioSchema = exports.updateAudioSchema = exports.createAudioSchema = void 0;
const zod_1 = require("zod");
const MAX_FILE_SIZE = 5000000;
const ACCEPTED_AUDIO_TYPES = [
    "image/jpeg",
    "image/jpg",
    "image/png",
    "image/webp",
    "audio/mpeg",
];
exports.createAudioSchema = zod_1.z.object({
    profileAudio: zod_1.z
        .any()
        .refine((files) => (files === null || files === void 0 ? void 0 : files.length) == 1, "Audio is required.")
        .refine((files) => { var _a; return ((_a = files === null || files === void 0 ? void 0 : files[0]) === null || _a === void 0 ? void 0 : _a.size) <= MAX_FILE_SIZE; }, `Max file size is 50MB.`)
        .refine((files) => { var _a; return ACCEPTED_AUDIO_TYPES.includes((_a = files === null || files === void 0 ? void 0 : files[0]) === null || _a === void 0 ? void 0 : _a.type); }, ".jpg, .jpeg, .png and .webp files are accepted."),
    title: (0, zod_1.string)({
        required_error: "title is required",
    }),
    duration: (0, zod_1.number)({
        required_error: "duration is required",
    }),
    category: (0, zod_1.string)({
        required_error: "Category is required",
    }),
    email: (0, zod_1.string)({
        required_error: "Email is required",
    }).email("Not a valid email"),
});
exports.updateAudioSchema = (0, zod_1.object)({
    params: (0, zod_1.object)({
        id: (0, zod_1.string)(),
    }),
    body: (0, zod_1.object)({
        name: (0, zod_1.string)({
            required_error: "Audio name is required",
        }),
    }),
});
exports.removeAudioSchema = (0, zod_1.object)({
    params: (0, zod_1.object)({
        id: (0, zod_1.string)(),
    }),
});
