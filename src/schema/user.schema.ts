import { object, string, TypeOf } from "zod";

/**
* @openapi
* components:
*  schemas:
*   CreateUserInput:
*     type: object
*     required:
*       - firstName
*       - lastName
*       - password
*       - passwordConfirmation
*       - email
*       - adresse
*       - phoneNumber
*     properties:
*       firstName:
*         type: string
*       lastName:
*         type: string
*       password:
*         type: string
*         minLength: 6
*       passwordConfirmation:
*         type: string
*         minLength: 6
*       email:
*         type: string
*         format: email
*       adresse:
*         type: string
*       phoneNumber:
*         type: string
*   CreateUserResponse:
*     type: object
*     properties:
*       code:
*         type: integer
*       message:
*         type: string
 */




export const createUserSchema = object({
  body: object({
    firstName: string({
      required_error: "First name is required",
    }),
    lastName: string({
      required_error: "Last name is required",
    }),
    password: string({
      required_error: "Password is required",
    }).min(6, "Password is too short - should be min 6 chars"),
    passwordConfirmation: string({
      required_error: "Password confirmation is required",
    }),
    email: string({
      required_error: "Email is required",
    }).email("Not a valid email"),
  }).refine((data) => data.password === data.passwordConfirmation, {
    message: "Passwords do not match",
    path: ["passwordConfirmation"],
  }),
});

/**
 * @openapi
 * components:
 *   schemas:
 *     VerifyUserParams:
 *       type: object
 *       properties:
 *         id:
 *           type: string
 *         verificationCode:
 *           type: string
 *       required:
 *         - id
 *         - verificationCode
 */
export const verifyUserSchema = object({
  params: object({
    id: string(),
    verificationCode: string(),
  }),
});

export const turnUsertoAdminSchema = object({
  params: object({
    id: string()
  }),
});



/**
 * @swagger
 * components:
 *   schemas:
 *     ForgotPasswordInput:
 *       type: object
 *       properties:
 *         email:
 *           type: string
 *           format : email
 *       required:
 *         - email
 */
export const forgotPasswordSchema = object({
  body: object({
    email: string({
      required_error: "Email is required",
    }).email("Not a valid email"),
  }),
});


/**
 * @swagger
 * components:
 *   schemas:
 *     ResetPasswordParams:
 *       type: object
 *       properties:
 *         id:
 *           type: string
 *         passwordResetCode:
 *           type: string
 *       required:
 *         - id
 *         - passwordResetCode
 *     ResetPasswordInput:
 *       type: object
 *       properties:
 *         password:
 *           type: string
 *         passwordConfirmation:
 *           type: string
 *       required:
 *         - password
 *         - passwordConfirmation
 */
export const resetPasswordSchema = object({
  params: object({
    id: string(),
    passwordResetCode: string(),
  }),
  body: object({
    password: string({
      required_error: "Password is required",
    }).min(6, "Password is too short - should be min 6 chars"),
    passwordConfirmation: string({
      required_error: "Password confirmation is required",
    }),
  }).refine((data) => data.password === data.passwordConfirmation, {
    message: "Passwords do not match",
    path: ["passwordConfirmation"],
  }),
});


/**
 * @openapi
 * components:
 *   schemas:
 *     ErrorResponse:
 *       type: object
 *       properties:
 *         errorMessage:
 *           type: string
 */

export type CreateUserInput = TypeOf<typeof createUserSchema>["body"]; // using zod I can create a typescript interface

export type VerifyUserInput = TypeOf<typeof verifyUserSchema>["params"];

export type ForgotPasswordInput = TypeOf<typeof forgotPasswordSchema>["body"];

export type ResetPasswordInput = TypeOf<typeof resetPasswordSchema>;
