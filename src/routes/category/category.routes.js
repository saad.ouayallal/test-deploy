"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const category_controller_1 = require("../../controllers/category.controller");
const requireUser_1 = require("../../middleware/requireUser");
const router = express_1.default.Router();
/**
  *@swagger
  * /api/category:
  *  post:
  *    summary: Add a new category
  *    tags: [Category]
  *    requestBody:
  *      required: true
  *      content:
  *        application/json:
  *          schema:
  *            type: object
  *            properties:
  *              title:
  *                type: string
  *            example:
  *              title: "category1"
  *    responses:
  *      '200':
  *        description: Category created successfully
  *        content:
  *          application/json:
  *            schema:
  *              type: object
  *              properties:
  *                code:
  *                  type: integer
  *                result:
  *                  type: object
  *                  properties:
  *                    title:
  *                      type: string
  *                    _id:
  *                      type: string
  *                    createdAt:
  *                      type: string
  *                      format: date-time
  *                    updatedAt:
  *                      type: string
  *                      format: date-time
  *                    __v:
  *                      type: integer
  *      '500':
  *        description: Internal server error
  *        content:
  *          application/json:
  *            schema:
  *              type: object
  *              properties:
  *                errorMessage:
  *                  type: string
 */
router.post("/api/category", requireUser_1.requireUserAdmin, category_controller_1.createCategory);
/**
* @swagger
* /api/category/{id}:
*   get:
*     summary: Get a category by Id
*     tags: [Category]
*     parameters:
*       - name: id
*         in: path
*         description: Category ID
*         required: true
*         schema:
*           type: string
*     responses:
*       '200':
*         description: Category retrieved successfully
*         content:
*           application/json:
*             schema:
*               type: object
*               properties:
*                 code:
*                   type: integer
*                 result:
*                   type: object
*                   properties:
*                     _id:
*                       type: string
*                     title:
*                       type: string
*                     createdAt:
*                       type: string
*                       format: date-time
*                     updatedAt:
*                       type: string
*                       format: date-time
*                     __v:
*                       type: integer
*       '500':
*         description: Internal server error
*         content:
*           application/json:
*             schema:
*               type: object
*               properties:
*                 errorMessage:
*                   type: string
*
*/
router.get("/api/category/:id", requireUser_1.requireUserAdmin, category_controller_1.getCategory);
/**
* @swagger
*  /api/category:
*    get:
*      summary: Get all categories
*      tags: [Category]
*      responses:
*        '200':
*          description: Categories retrieved successfully
*          content:
*            application/json:
*              schema:
*                type: object
*                properties:
*                  code:
*                    type: integer
*                  result:
*                    type: array
*                    items:
*                      type: object
*                      properties:
*                        _id:
*                          type: string
*                        title:
*                          type: string
*                        createdAt:
*                          type: string
*                          format: date-time
*                        updatedAt:
*                          type: string
*                          format: date-time
*                        __v:
*                          type: integer
*        '500':
*          description: Internal server error
*          content:
*            application/json:
*              schema:
*                type: object
*                properties:
*                  errorMessage:
*                    type: string
*/
router.get("/api/category", requireUser_1.requireUserAdmin, category_controller_1.getallCategory);
/**
  *@swagger
  * /api/category/{id}:
  *  put:
  *    summary: update a category
  *    tags: [Category]
  *    parameters:
  *      - name: id
  *        in: path
  *        description: Category ID
  *        required: true
  *        schema:
  *          type: string
  *    requestBody:
  *      required: true
  *      content:
  *        application/json:
  *          schema:
  *            type: object
  *            properties:
  *              title:
  *                type: string
  *            example:
  *              title: "category1"
  *    responses:
  *      '200':
  *        description: Category created successfully
  *        content:
  *          application/json:
  *            schema:
  *              type: object
  *              properties:
  *                code:
  *                  type: integer
  *                result:
  *                  type: object
  *                  properties:
  *                    title:
  *                      type: string
  *                    _id:
  *                      type: string
  *                    createdAt:
  *                      type: string
  *                      format: date-time
  *                    updatedAt:
  *                      type: string
  *                      format: date-time
  *                    __v:
  *                      type: integer
  *      '500':
  *        description: Internal server error
  *        content:
  *          application/json:
  *            schema:
  *              type: object
  *              properties:
  *                errorMessage:
  *                  type: string
 */
router.put("/api/category/:id", requireUser_1.requireUserAdmin, category_controller_1.updateCategory);
/**
* @swagger
* /api/category/{id}:
*    delete:
*      summary: Delete a category
*      tags: [Category]
*      parameters:
*        - in: path
*          name: id
*          schema:
*            type: string
*          required: true
*          description: ID of the category to delete
*      responses:
*        '200':
*          description: Category deleted successfully
*          content:
*            application/json:
*              schema:
*                type: object
*                properties:
*                  code:
*                    type: integer
*                  message:
*                    type: string
*        '404':
*          description: Category not found
*          content:
*            application/json:
*              schema:
*                type: object
*                properties:
*                  errorMessage:
*                    type: string
*        '500':
*          description: Internal server error
*          content:
*            application/json:
*              schema:
*                type: object
*                properties:
*                  errorMessage:
*                    type: string
*/
router.delete("/api/category/:id", requireUser_1.requireUserAdmin, category_controller_1.deleteCategory);
exports.default = router;
