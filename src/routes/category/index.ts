import express from "express";
import category from "./category.routes";

const router = express.Router();

router.get("/healthcheck", (_, res) => res.sendStatus(200));

router.use(category);

export default router;
