import express from "express";
import {
  createAdminSessionHandler,
  createUserSessionHandler,
  getUserSessionsHandler,
  signOutHandler,
} from "../../controllers/auth.controller";
import deserializeUser from "../../middleware/deserializeUser";
import { requireUser } from "../../middleware/requireUser";
import validateResource from "../../middleware/validateResource";
import { createSessionSchema } from "../../schema/auth.schema";

const router = express.Router();


router.post(
  "/api/sessions",
  validateResource(createSessionSchema),
  createUserSessionHandler
);

/**
 * @swagger
 * /api/sessions/admin:
 *   post:
 *     summary: Create an Admin Session
 *     tags: [Session]
 *     requestBody:
 *      required: true
 *      content:
 *        application/json:
 *           schema:
 *              $ref: '#/components/schemas/createSessionInput'
 *     responses:
 *       200:
 *         description: Successful response with access and refresh tokens
 *         content:
 *           application/json:
 *             schema:
 *              $ref: '#/components/schemas/createSessionResponse'
 *       400:
 *         description: Invalid email or password
 *         content:
 *           application/json:
 *             schema:
 *              $ref: '#/components/schemas/ErrorResponse'
 *       401:
 *         description: User is not an Admin
 *         content:
 *           application/json:
 *             schema:
 *              $ref: '#/components/schemas/ErrorResponse'
 *       500:
 *         description: Invalid email or password
 *         content:
 *           application/json:
 *            example:
 *               errorMessage: Internal error
 */
router.post(
  "/api/sessions/admin",
  validateResource(createSessionSchema),
  createAdminSessionHandler
);



/**
   * @swagger
   * '/api/sessions':
   *  get:
   *    tags:
   *    - Session
   *    summary: Get all sessions
   *    responses:
   *      200:
   *        description: Get all sessions for current user
   *        content:
   *          application/json:
   *            schema:
   *              $ref: '#/components/schemas/GetSessionResponse'
   *      403:
   *        description: Forbidden
   *  post:
   *    tags:
   *    - Session
   *    summary: Create a session
   *    requestBody:
   *      required: true
   *      content:
   *        application/json:
   *          schema:
   *            $ref: '#/components/schemas/CreateSessionInput'
   *    responses:
   *      200:
   *        description: Session created
   *        content:
   *          application/json:
   *            schema:
   *              $ref: '#/components/schemas/CreateSessionResponse'
   *      401:
   *        description: Unauthorized
   *  delete:
   *    tags:
   *    - Session
   *    summary: Delete a session
   *    responses:
   *      200:
   *        description: Session deleted
   *      403:
   *        description: Forbidden
   */
router.get("/api/sessions", requireUser, getUserSessionsHandler)


router.delete("/api/sessions", requireUser, signOutHandler);

export default router;
