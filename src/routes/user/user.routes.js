"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const user_controller_1 = require("../../controllers/user.controller");
const requireUser_1 = require("../../middleware/requireUser");
const validateResource_1 = __importDefault(require("../../middleware/validateResource"));
const user_schema_1 = require("../../schema/user.schema");
const router = express_1.default.Router();
/**
 * @openapi
 * '/api/users':
 *  post:
 *     summary: Register a user
 *     tags: [Users]
 *     requestBody:
 *      required: true
 *      content:
 *        application/json:
 *           schema:
 *              $ref: '#/components/schemas/CreateUserInput'
 *     responses:
 *       200:
 *         description: User successfully created
 *         content:
 *           application/json:
 *             example:
 *               code: 200
 *               message: User successfully created
 *       409:
 *         description: Account already exists
 *         content:
 *           application/json:
 *             schema:
 *              $ref: '#/components/schemas/ErrorResponse'
 *       500:
 *         description: Internal server error
 *         content:
 *           application/json:
 *             example:
 *               errorMessage: Internal error
 */
router.post("/api/users", (0, validateResource_1.default)(user_schema_1.createUserSchema), user_controller_1.createUserHandler);
/**
 *@openapi
 * paths:
 *  /api/users/verify/{id}/{verificationCode}:
 *    post:
 *      summary: Verify user account
 *      tags:
 *        - Users
 *      parameters:
 *        - in: path
 *          name: id
 *          required: true
 *          minLength: 24
 *          schema:
 *            type: string
 *          description: User's ID
 *        - in: path
 *          name: verificationCode
 *          required: true
 *          minLength: 21
 *          schema:
 *            type: string
 *          description: Verification code
 *      responses:
 *        200:
 *          description: User account verified successfully
 *          content:
 *            application/json:
 *              example:
 *                code: 200
 *                message: User successfully verified
 *        400:
 *          description: Bad request or verification code mismatch
 *          content:
 *            application/json:
 *              schema:
 *               $ref: '#/components/schemas/ErrorResponse'
 *        404:
 *          description: User not found
 *          content:
 *            application/json:
 *              schema:
 *               $ref: '#/components/schemas/ErrorResponse'
 *        500:
 *          description: Internal server error
 *          content:
 *            application/json:
 *              example:
 *                errorMessage: Internal server error
 */
router.post("/api/users/verify/:id/:verificationCode", (0, validateResource_1.default)(user_schema_1.verifyUserSchema), user_controller_1.verifyUserHandler);
/**
 * @openapi
 * /api/users/forgot-password:
 *   post:
 *     summary: Initiate forgot password process
 *     tags: [Users]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/ForgotPasswordInput'
 *     responses:
 *       200:
 *         description: Reset password email sent successfully
 *         content:
 *         application/json:
 *           example:
 *             code: 200
 *             message: Password reset email sent to ${email}
 *       404:
 *         description: User with 'email' does not exist
 *         content:
 *           application/json:
 *             schema:
 *              $ref: '#/components/schemas/ErrorResponse'
 *       400:
 *         description: User is not verified
 *         content:
 *           application/json:
 *             schema:
 *              $ref: '#/components/schemas/ErrorResponse'
 *       500:
 *         description: Internal server error
 *         content:
 *         application/json:
 *           example:
 *             errorMessage: Internal error
 */
router.post("/api/users/forgotpassword", (0, validateResource_1.default)(user_schema_1.forgotPasswordSchema), user_controller_1.forgotPasswordHandler);
/**
 * @openapi
 * /api/users/reset-password/{id}/{passwordResetCode}:
 *   post:
 *     summary: Reset user password
 *     tags: [Users]
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: User's ID
 *         schema:
 *           type: string
 *       - in: path
 *         name: passwordResetCode
 *         required: true
 *         description: Password reset code
 *         schema:
 *           type: string
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/ResetPasswordInput'
 *     responses:
 *       200:
 *         description: Successfully updated password
 *         content:
 *         application/json:
 *           example:
 *             code: 200
 *             message: Successfully updated password
 *       400:
 *         description: Could not reset user password because user not found OR passwordResetCode is not passed or incorrect
 *         content:
 *           application/json:
 *             schema:
 *              $ref: '#/components/schemas/ErrorResponse'
 *       500:
 *         description: Internal server error
 *         content:
 *         application/json:
 *           example:
 *             errorMessage: Internal server error
 */
router.post("/api/users/resetpassword/:id/:passwordResetCode", (0, validateResource_1.default)(user_schema_1.resetPasswordSchema), user_controller_1.resetPasswordHandler);
/**
 * @swagger
 * /api/users/admin/{id}:
 *   post:
 *     summary: Turn a user into an admin
 *     tags: [Users]
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: ID of the user to turn into an admin
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: User successfully turned into an admin
 *         content:
 *           application/json:
 *             example:
 *               code: 200
 *               message: User successfully turned to Admin
 *       400:
 *         description: Could not verify user
 *         content:
 *           application/json:
 *             example:
 *               errorMessage: Could not verify user
 *       401:
 *         description: User is already an admin
 *         content:
 *           application/json:
 *             example:
 *               errorMessage: User is Already has the privilege of an Admin
 *       500:
 *         description: Could not give the privileges of an admin
 *         content:
 *           application/json:
 *             example:
 *               errorMessage: Could not give the privileges of an admin
 */
router.post("/api/users/admin/:id", (0, validateResource_1.default)(user_schema_1.turnUsertoAdminSchema), requireUser_1.requireUserAdmin, user_controller_1.turnUsertoAdminHandler);
router.get("/api/users/me", requireUser_1.requireUser, user_controller_1.getCurrentUserHandler);
/**
 * @swagger
 * /api/users/delete/{id}:
 *   delete:
 *     summary: Delete a user
 *     tags: [Users]
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: ID of the user to delete
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: User deleted successfully
 *         content:
 *           application/json:
 *             example:
 *               code: 200
 *               message: User <user> has been deleted
 *       401:
 *         description: Unauthorized or missing authentication
 *         content:
 *           application/json:
 *             schema:
 *              $ref: '#/components/schemas/ErrorResponse'
 *       500:
 *         description: Internal server error
 *         content:
 *           application/json:
 *             schema:
 *              $ref: '#/components/schemas/ErrorResponse'
 */
router.delete("/api/users/delete/:id", requireUser_1.requireUserAdmin, user_controller_1.deleteUserHandler);
/**
 * @swagger
 * /api/users:
 *   get:
 *     summary: Get all users
 *     tags: [Users]
 *     responses:
 *       200:
 *         description: List of all users retrieved successfully
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/getAllUsersResponse'
 *       401:
 *         description: Unauthorized or missing authentication
 *         content:
 *           application/json:
 *             schema:
 *              $ref: '#/components/schemas/ErrorResponse'
 *       500:
 *         description: Internal server error
 *         content:
 *           application/json:
 *             schema:
 *              $ref: '#/components/schemas/ErrorResponse'
 */
/**
 * @swagger
 * components:
 *   schemas:
 *     getAllUsersResponse:
 *       type: object
 *       properties:
 *         _id:
 *           type: string
 *           description: ID of the user
 *         email:
 *           type: string
 *           description: Email of the user
 *         firstName:
 *           type: string
 *           description: First name of the user
 *         lastName:
 *           type: string
 *           description: Last name of the user
 *         adresse:
 *           type: string
 *           description: Address of the user
 *         phoneNumber:
 *           type: number
 *           description: Phone number of the user
 *         verified:
 *           type: boolean
 *           description: Indicates if the user is verified
 *         isAdmin:
 *           type: boolean
 *           description: Indicates if the user is an admin
 *         cart:
 *           type: array
 *           audio:
 *             type: object
 *             description: audios in the user's cart
 *         wishlist:
 *           type: array
 *           items:
 *             type: object
 *             description: Item in the user's wishlist
 *         verificationCode:
 *           type: string
 *           description: Verification code of the user
 *         createdAt:
 *           type: string
 *           format: date-time
 *           description: Date and time when the user was created
 *         updatedAt:
 *           type: string
 *           format: date-time
 *           description: Date and time when the user was last updated
 *         __v:
 *           type: number
 */
router.get("/api/users", requireUser_1.requireUserAdmin, user_controller_1.getAllUsersHandler);
/**
 * @swagger
 *  /api/user/wishlist/{id}:
 *    get:
 *      summary: Get user's wishlist
 *      tags: [Users]
 *      parameters:
 *        - in: path
 *          name: id
 *          required: true
 *          schema:
 *            type: string
 *          description: User ID
 *      responses:
 *        '200':
 *          description: Wishlist retrieved successfully
 *          content:
 *            application/json:
 *              schema:
 *                type: array
 *                items:
 *                  type: object
 *                  properties:
 *                    _id:
 *                      type: string
 *                    filename:
 *                      type: string
 *                    mimetype:
 *                      type: string
 *                    title:
 *                      type: string
 *                    category:
 *                      type: array
 *                      items:
 *                        type: string
 *                    description:
 *                      type: string
 *                    price:
 *                      type: number
 *                    ratings:
 *                      type: array
 *                      items:
 *                        type: object
 *                        properties:
 *                          postedby:
 *                            type: string
 *                          _id:
 *                            type: string
 *                          createdAt:
 *                            type: string
 *                            format: date-time
 *                          updatedAt:
 *                            type: string
 *                            format: date-time
 *                    totalrating:
 *                      type: integer
 *                    duration:
 *                      type: integer
 *                    uploaded:
 *                      type: string
 *                      format: date-time
 *                    __v:
 *                      type: integer
 *        '500':
 *          description: Internal server error
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  errorMessage:
 *                    type: string
 */
router.get("/api/user/wishlist/:id", requireUser_1.requireUser, user_controller_1.getWishlistHandler);
/**
 *@swagger
 *  /api/cart/{id}:
 *    post:
 *      summary: Add to Cart
 *      tags: [Cart]
 *      parameters:
 *        - in: path
 *          name: id
 *          required: true
 *          schema:
 *            type: string
 *          description: User ID
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                cart:
 *                  type: array
 *                  items:
 *                    type: object
 *                    properties:
 *                      id:
 *                        type: string
 *                      price:
 *                        type: number
 *              example:
 *                cart:
 *                  - id: "64ee88f1a1d17bf2ab9f886d"
 *                    price: 50
 *                  - id: "64ee88f1a1d17bf2ab9f886c"
 *                    price: 30
 *      responses:
 *        '200':
 *          description: Cart updated successfully
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  products:
 *                    type: array
 *                    items:
 *                      type: object
 *                      properties:
 *                        product:
 *                          type: string
 *                        price:
 *                          type: number
 *                        _id:
 *                          type: string
 *                        createdAt:
 *                          type: string
 *                          format: date-time
 *                        updatedAt:
 *                          type: string
 *                          format: date-time
 *                  cartTotal:
 *                    type: number
 *                  orderby:
 *                    type: string
 *                  _id:
 *                    type: string
 *                  __v:
 *                    type: integer
 *        '500':
 *          description: Internal server error
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  errorMessage:
 *                    type: string
 */
router.post("/api/cart/:id", requireUser_1.requireUser, user_controller_1.userCart);
/**
 *  @swagger
 *   /api/cart/{id}:
 *     get:
 *       summary: Get the Cart
 *       tags: [Cart]
 *       parameters:
 *         - in: path
 *           name: id
 *           required: true
 *           schema:
 *             type: string
 *           description: User ID
 *       responses:
 *         '200':
 *           description: Cart retrieved successfully
 *           content:
 *             application/json:
 *               schema:
 *                 type: object
 *                 properties:
 *                   _id:
 *                     type: string
 *                   products:
 *                     type: array
 *                     items:
 *                       type: object
 *                       properties:
 *                         product:
 *                           type: object
 *                           properties:
 *                             _id:
 *                               type: string
 *                             filename:
 *                               type: string
 *                             mimetype:
 *                               type: string
 *                             title:
 *                               type: string
 *                             category:
 *                               type: array
 *                               items:
 *                                 type: string
 *                             description:
 *                               type: string
 *                             price:
 *                               type: number
 *                             totalrating:
 *                               type: integer
 *                             duration:
 *                               type: integer
 *                             uploaded:
 *                               type: string
 *                               format: date-time
 *                             ratings:
 *                               type: array
 *                               items:
 *                                 type: object
 *                                 properties:
 *                                   postedby:
 *                                     type: string
 *                                   _id:
 *                                     type: string
 *                                   createdAt:
 *                                     type: string
 *                                     format: date-time
 *                                   updatedAt:
 *                                     type: string
 *                                     format: date-time
 *                         price:
 *                           type: number
 *                         _id:
 *                           type: string
 *                         createdAt:
 *                           type: string
 *                           format: date-time
 *                         updatedAt:
 *                           type: string
 *                           format: date-time
 *                   cartTotal:
 *                     type: number
 *                   orderby:
 *                     type: string
 *                   __v:
 *                     type: integer
 *         '500':
 *           description: Internal server error
 *           content:
 *             application/json:
 *               schema:
 *                 type: object
 *                 properties:
 *                   errorMessage:
 *                     type: string
 */
router.get("/api/cart/:id", requireUser_1.requireUser, user_controller_1.getUserCart);
/**
 *  @swagger
 *   /api/empty-cart/{id}:
 *     delete:
 *       summary: Delete the Cart
 *       tags: [Cart]
 *       parameters:
 *         - in: path
 *           name: id
 *           required: true
 *           schema:
 *             type: string
 *           description: User ID
 *       responses:
 *         '200':
 *           description: Cart deleted successfully
 *           content:
 *             application/json:
 *               schema:
 *                 type: object
 *                 properties:
 *                   _id:
 *                     type: string
 *                   products:
 *                     type: array
 *                     items:
 *                       type: object
 *                       properties:
 *                         product:
 *                           type: string
 *                         price:
 *                           type: number
 *                         _id:
 *                           type: string
 *                         createdAt:
 *                           type: string
 *                           format: date-time
 *                         updatedAt:
 *                           type: string
 *                           format: date-time
 *                   cartTotal:
 *                     type: number
 *                   orderby:
 *                     type: string
 *                   __v:
 *                     type: integer
 *         '500':
 *           description: Internal server error
 *           content:
 *             application/json:
 *               schema:
 *                 type: object
 *                 properties:
 *                   errorMessage:
 *                     type: string
 */
router.delete("/api/empty-cart/:id", requireUser_1.requireUser, user_controller_1.emptyUserCart);
/**
 *@swagger
 *  /api/cart/{id}:
 *    post:
 *      summary: Add to Cart
 *      tags: [Cart]
 *      parameters:
 *        - in: path
 *          name: id
 *          required: true
 *          schema:
 *            type: string
 *          description: User ID
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                cart:
 *                  type: array
 *                  items:
 *                    type: object
 *                    properties:
 *                      id:
 *                        type: string
 *                      price:
 *                        type: number
 *              example:
 *                cart:
 *                  - id: "64ee88f1a1d17bf2ab9f886d"
 *                    price: 50
 *                  - id: "64ee88f1a1d17bf2ab9f886c"
 *                    price: 30
 *      responses:
 *        '200':
 *          description: Cart updated successfully
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  products:
 *                    type: array
 *                    items:
 *                      type: object
 *                      properties:
 *                        product:
 *                          type: string
 *                        price:
 *                          type: number
 *                        _id:
 *                          type: string
 *                        createdAt:
 *                          type: string
 *                          format: date-time
 *                        updatedAt:
 *                          type: string
 *                          format: date-time
 *                  cartTotal:
 *                    type: number
 *                  orderby:
 *                    type: string
 *                  _id:
 *                    type: string
 *                  __v:
 *                    type: integer
 *        '500':
 *          description: Internal server error
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  errorMessage:
 *                    type: string
 */
router.post("/api/cart/cash-order/:id", requireUser_1.requireUser, user_controller_1.createOrder);
/**
 *@swagger
 *  /api/cart/{id}:
 *    post:
 *      summary: Add to Cart
 *      tags: [Cart]
 *      parameters:
 *        - in: path
 *          name: id
 *          required: true
 *          schema:
 *            type: string
 *          description: User ID
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                cart:
 *                  type: array
 *                  items:
 *                    type: object
 *                    properties:
 *                      id:
 *                        type: string
 *                      price:
 *                        type: number
 *              example:
 *                cart:
 *                  - id: "64ee88f1a1d17bf2ab9f886d"
 *                    price: 50
 *                  - id: "64ee88f1a1d17bf2ab9f886c"
 *                    price: 30
 *      responses:
 *        '200':
 *          description: Cart updated successfully
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  products:
 *                    type: array
 *                    items:
 *                      type: object
 *                      properties:
 *                        product:
 *                          type: string
 *                        price:
 *                          type: number
 *                        _id:
 *                          type: string
 *                        createdAt:
 *                          type: string
 *                          format: date-time
 *                        updatedAt:
 *                          type: string
 *                          format: date-time
 *                  cartTotal:
 *                    type: number
 *                  orderby:
 *                    type: string
 *                  _id:
 *                    type: string
 *                  __v:
 *                    type: integer
 *        '500':
 *          description: Internal server error
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  errorMessage:
 *                    type: string
 */
router.get("/api/cart/get-order/:id", requireUser_1.requireUser, user_controller_1.getOrderByUserId);
router.get("/api/getOrders", requireUser_1.requireUserAdmin, user_controller_1.getAllOrders);
/**
 *@swagger
 *  /api/cart/{id}:
 *    post:
 *      summary: Add to Cart
 *      tags: [Cart]
 *      parameters:
 *        - in: path
 *          name: id
 *          required: true
 *          schema:
 *            type: string
 *          description: User ID
 *      requestBody:
 *        required: true
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                cart:
 *                  type: array
 *                  items:
 *                    type: object
 *                    properties:
 *                      id:
 *                        type: string
 *                      price:
 *                        type: number
 *              example:
 *                cart:
 *                  - id: "64ee88f1a1d17bf2ab9f886d"
 *                    price: 50
 *                  - id: "64ee88f1a1d17bf2ab9f886c"
 *                    price: 30
 *      responses:
 *        '200':
 *          description: Cart updated successfully
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  products:
 *                    type: array
 *                    items:
 *                      type: object
 *                      properties:
 *                        product:
 *                          type: string
 *                        price:
 *                          type: number
 *                        _id:
 *                          type: string
 *                        createdAt:
 *                          type: string
 *                          format: date-time
 *                        updatedAt:
 *                          type: string
 *                          format: date-time
 *                  cartTotal:
 *                    type: number
 *                  orderby:
 *                    type: string
 *                  _id:
 *                    type: string
 *                  __v:
 *                    type: integer
 *        '500':
 *          description: Internal server error
 *          content:
 *            application/json:
 *              schema:
 *                type: object
 *                properties:
 *                  errorMessage:
 *                    type: string
 */
router.put("/api/cart/update-order-status/:id", requireUser_1.requireUserAdmin, user_controller_1.UpdateOrderStatus);
exports.default = router;
