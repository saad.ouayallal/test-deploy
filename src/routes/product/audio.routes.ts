import express from "express";
import {
  createAudioHandler,
  updateAudioHandler,
  removeAudioHandler,
  getAudioHandler,
  getAllAudiosHandler,
  addToWishlist,
  addCategoryToAudio,
} from "../../controllers/audio.controller";
import { requireUser, requireUserAdmin } from "../../middleware/requireUser";
import validateResource from "../../middleware/validateResource";

const router = express.Router();

/**
 * @swagger
 * /api/audios:
 *  post:
 *    summary: Add a new audio file
 *    tags: [Audio]
 *    requestBody:
 *      required: true
 *      content:
 *        multipart/form-data:
 *          schema:
 *            type: object
 *            properties:
 *              audio:
 *                type: string
 *                format: binary
 *              title:
 *                type: string
 *              duration:
 *                type: integer
 *              category:
 *                type: string
 *              description:
 *                type: string
 *              price:
 *                type: number
 *    responses:
 *      '200':
 *        description: Audio file created successfully
 *        content:
 *          application/json:
 *            schema:
 *              type: object
 *              properties:
 *                id:
 *                  type: string
 *                  example: "64ee6ae581e2e733f94d7846"
 *                result:
 *                  type: object
 *                  properties:
 *                    filename:
 *                      type: string
 *                    mimetype:
 *                      type: string
 *                    title:
 *                      type: string
 *                    category:
 *                      type: string
 *                    description:
 *                      type: string
 *                    price:
 *                      type: number
 *                    images:
 *                      type: array
 *                      items:
 *                        type: string
 *                    ratings:
 *                      type: array
 *                      items:
 *                        type: object
 *                        properties:
 *                          postedby:
 *                            type: string
 *                          _id:
 *                            type: string
 *                    totalrating:
 *                      type: integer
 *                    duration:
 *                      type: integer
 *                    uploaded:
 *                      type: string
 *                      format: date-time
 *                    _id:
 *                      type: string
 *                    __v:
 *                      type: integer
 *      400:
 *        description: File not uploaded or bad request
 *        content:
 *          application/json:
 *            example:
 *              errorMessage: File not uploaded
 *      500:
 *        description: Internal server error
 *        content:
 *          application/json:
 *            example:
 *              errorMessage: Internal error
 */
router.post("/api/audios", requireUserAdmin, createAudioHandler);

/**
 * @swagger
 * /api/audios/{id}:
 *   get:
 *     summary: Get audio by ID
 *     tags: [Audio]
 *     parameters:
 *       - in: path
 *         name: id
 *         required: true
 *         description: ID of the audio to retrieve
 *         schema:
 *           type: string
 *     responses:
 *       200:
 *         description: Audio retrieved successfully
 *       404:
 *         description: Audio not found
 *         content:
 *           application/json:
 *             example:
 *               errorMessage: Audio not found
 *       500:
 *         description: Internal server error
 *         content:
 *           application/json:
 *             example:
 *               errorMessage: Internal Error
 */
router.get(
  "/api/audios/:id",
  requireUser,
  getAudioHandler
);

/**
 * @swagger
 * /api/audios:
 *   get:
 *     summary: Get all audios
 *     tags: [Audio]
 *     responses:
 *       '200':
 *         description: List of all audio files retrieved successfully
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 audios:
 *                   type: array
 *                   items:
 *                     $ref: '#/components/schemas/Audios'
 *       '404':
 *         description: Audios not found
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 errorMessage:
 *                   type: string
 *       '500':
 *         description: Internal server error
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 errorMessage:
 *                   type: string
 *
 *
 */
/**
 * @swagger
  * components:
  *   schemas:
  *    Audios:
  *      type: object
  *      properties:
  *        _id:
  *          type: string
  *        filename:
  *          type: string
  *        mimetype:
  *          type: string
  *        title:
  *          type: string
  *        description:
  *          type: string
  *        price:
  *          type: number
  *        ratings:
  *          type: array
  *          items:
  *            $ref: '#/components/schemas/Rating'
  *        totalrating:
  *          type: number
  *        duration:
  *          type: number
  *        uploaded:
  *          type: string
  *          format: date-time
  *        __v:
  *          type: integer
  *        images:
  *          type: array
  *          items:
  *            type: string
  *    Rating:
  *      type: object
  *      properties:
  *        postedby:
  *          type: string
  *        _id:
  *          type: string
  *        createdAt:
  *          type: string
  *          format: date-time
  *        updatedAt:
  *          type: string
  *          format: date-time

 */
router.get(
  "/api/audios",
  requireUser,
  getAllAudiosHandler
);

router.put(
  "/api/audios/:id",
  requireUserAdmin,
  updateAudioHandler
);

/**
 * @swagger
 * /api/audios/{id}:
 *   delete:
 *     summary: Remove an audio file
 *     tags: [Audio]
 *     parameters:
 *       - name: id
 *         in: path
 *         description: ID of the audio file to remove
 *         required: true
 *         schema:
 *           type: string
 *     responses:
 *       '200':
 *         description: Audio file removed successfully
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 code:
 *                   type: integer
 *                   example: 200
 *                 message:
 *                   type: string
 *                   example: Audio successfully removed
 *       '404':
 *         description: Audio not found or file already deleted
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 errorMessage:
 *                   type: string
 *       '500':
 *         description: Internal server error
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 errorMessage:
 *                   type: string
 */
router.delete(
  "/api/audios/:id",
  requireUserAdmin,
  removeAudioHandler
);

router.post(
  "/api/audios/wishlist",
  requireUser,
  addToWishlist
);

router.post(
  "/api/audios/category",
  requireUserAdmin,
  addCategoryToAudio
);

export default router;
