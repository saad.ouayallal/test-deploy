import express from "express";
import audio from "./audio.routes";

const router = express.Router();

router.get("/healthcheck", (_, res) => res.sendStatus(200));

router.use(audio);

export default router;
