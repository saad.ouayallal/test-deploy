import {
  getModelForClass,
  modelOptions,
  prop,
  Severity,
  Ref,
  index,
} from "@typegoose/typegoose";
import { ObjectId } from "mongoose";
import { User, Category } from "./index";

@index({ email: 1 })
@modelOptions({
  schemaOptions: {
    timestamps: true, // to add timestamps like createdAt & updatedAt
  },
  options: {
    allowMixed: Severity.ALLOW,
  },
})

class Rating {
  @prop()
  public star!: number;

  @prop()
  public comment!: string;

  @prop({ ref:  'User' })
  public postedby!: Ref<User>;
}
class Audio {
  @prop({ required: true })
  filename: string;

  @prop()
  mimetype: string;

  @prop({ required: true })
  title: string;

  @prop({ ref: "Category" })
  category: Array<Category>;

  @prop({ required: true })
  description: string;

  @prop({ required: true })
  price: number;

  @prop({ type: () => [Rating] })
  public ratings!: Rating[];

  @prop({ default: 0 })
  public totalrating?: number;

  @prop({ required: true })
  duration: number;

  @prop({ required: false })
  uploaded: Date;
}

const AudioModel = getModelForClass(Audio);

export { Audio, AudioModel };
