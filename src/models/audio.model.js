"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AudioModel = exports.Audio = void 0;
const typegoose_1 = require("@typegoose/typegoose");
let Rating = class Rating {
};
__decorate([
    (0, typegoose_1.prop)(),
    __metadata("design:type", Number)
], Rating.prototype, "star", void 0);
__decorate([
    (0, typegoose_1.prop)(),
    __metadata("design:type", String)
], Rating.prototype, "comment", void 0);
__decorate([
    (0, typegoose_1.prop)({ ref: 'User' }),
    __metadata("design:type", Object)
], Rating.prototype, "postedby", void 0);
Rating = __decorate([
    (0, typegoose_1.index)({ email: 1 }),
    (0, typegoose_1.modelOptions)({
        schemaOptions: {
            timestamps: true, // to add timestamps like createdAt & updatedAt
        },
        options: {
            allowMixed: typegoose_1.Severity.ALLOW,
        },
    })
], Rating);
class Audio {
}
exports.Audio = Audio;
__decorate([
    (0, typegoose_1.prop)({ required: true }),
    __metadata("design:type", String)
], Audio.prototype, "filename", void 0);
__decorate([
    (0, typegoose_1.prop)(),
    __metadata("design:type", String)
], Audio.prototype, "mimetype", void 0);
__decorate([
    (0, typegoose_1.prop)({ required: true }),
    __metadata("design:type", String)
], Audio.prototype, "title", void 0);
__decorate([
    (0, typegoose_1.prop)({ ref: "Category" }),
    __metadata("design:type", Array)
], Audio.prototype, "category", void 0);
__decorate([
    (0, typegoose_1.prop)({ required: true }),
    __metadata("design:type", String)
], Audio.prototype, "description", void 0);
__decorate([
    (0, typegoose_1.prop)({ required: true }),
    __metadata("design:type", Number)
], Audio.prototype, "price", void 0);
__decorate([
    (0, typegoose_1.prop)({ type: () => [Rating] }),
    __metadata("design:type", Array)
], Audio.prototype, "ratings", void 0);
__decorate([
    (0, typegoose_1.prop)({ default: 0 }),
    __metadata("design:type", Number)
], Audio.prototype, "totalrating", void 0);
__decorate([
    (0, typegoose_1.prop)({ required: true }),
    __metadata("design:type", Number)
], Audio.prototype, "duration", void 0);
__decorate([
    (0, typegoose_1.prop)({ required: false }),
    __metadata("design:type", Date)
], Audio.prototype, "uploaded", void 0);
const AudioModel = (0, typegoose_1.getModelForClass)(Audio);
exports.AudioModel = AudioModel;
