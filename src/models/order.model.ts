import {
  getModelForClass,
  modelOptions,
  prop,
  Severity,
  Ref,
  index,
} from "@typegoose/typegoose";
import { ObjectId } from "mongoose";
import { User, Audio } from "./index";

@index({ email: 1 })
@modelOptions({
  schemaOptions: {
    timestamps: true, // to add timestamps like createdAt & updatedAt
  },
  options: {
    allowMixed: Severity.ALLOW,
  },
})
class ProductInfo {
  @prop({ ref: "Audio" })
  public product!: Ref<Audio>;

  // maybe will add other variables here like color and count in the future
}
class Order {
  @prop({ type: () => [ProductInfo] })
  public products!: ProductInfo[];

  @prop()
  public paymentIntent!: Record<string, unknown>;

  @prop({
    enum: [
      "Not Processed",
      "Payment Online",
      "Processing",
      "Dispatched",
      "Cancelled",
      "Delivered",
    ],
    default: "Not Processed",
  })
  public orderStatus!: string;
  @prop({ ref: () => User })
  public orderby!: Ref<User>;
}

const OrderModel = getModelForClass(Order);

export default OrderModel;
