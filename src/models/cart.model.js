"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Cart = void 0;
const typegoose_1 = require("@typegoose/typegoose");
const user_model_1 = require("./user.model");
const audio_model_1 = require("./audio.model");
let ProductInCart = class ProductInCart {
};
__decorate([
    (0, typegoose_1.prop)({ ref: () => audio_model_1.Audio }),
    __metadata("design:type", Object)
], ProductInCart.prototype, "product", void 0);
__decorate([
    (0, typegoose_1.prop)(),
    __metadata("design:type", Number)
], ProductInCart.prototype, "price", void 0);
ProductInCart = __decorate([
    (0, typegoose_1.index)({ email: 1 }),
    (0, typegoose_1.modelOptions)({
        schemaOptions: {
            timestamps: true, // to add timestamps like createdAt & updatedAt
        },
        options: {
            allowMixed: typegoose_1.Severity.ALLOW,
        },
    })
], ProductInCart);
class Cart {
}
exports.Cart = Cart;
__decorate([
    (0, typegoose_1.prop)({ type: () => [ProductInCart] }),
    __metadata("design:type", Array)
], Cart.prototype, "products", void 0);
__decorate([
    (0, typegoose_1.prop)(),
    __metadata("design:type", Number)
], Cart.prototype, "cartTotal", void 0);
__decorate([
    (0, typegoose_1.prop)({ ref: () => user_model_1.User }),
    __metadata("design:type", Object)
], Cart.prototype, "orderby", void 0);
const CartModel = (0, typegoose_1.getModelForClass)(Cart);
exports.default = CartModel;
