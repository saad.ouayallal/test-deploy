import {
  getModelForClass,
  modelOptions,
  prop,
  Severity,
  pre,
  DocumentType,
  index,
} from "@typegoose/typegoose";
import { nanoid } from "nanoid";
import argon2 from "argon2";
import log from "../db/logger";
import { Audio } from "./audio.model";

export const privateFields = [
  "password",
  "__v",
  "verificationCode",
  "passwordResetCode",
  "verified",
];

@pre<User>("save", async function () {
  // before saving the user's password we hash it
  if (!this.isModified("password")) {
    return;
  }

  const hash = await argon2.hash(this.password); // using argon2 instead of bcrypt for security reasons

  this.password = hash;

  return;
})
@index({ email: 1 })
@modelOptions({
  schemaOptions: {
    timestamps: true, // to add timestamps like createdAt & updatedAt
  },
  options: {
    allowMixed: Severity.ALLOW,
  },
})
class User {
  @prop({ lowercase: true, required: true, unique: true })
  email: string;

  @prop({ required: true })
  firstName: string;

  @prop({ required: true })
  lastName: string;

  @prop({ required: true })
  password: string;

  @prop({ required: true })
  adresse: string;

  @prop({ required: true })
  phoneNumber: number;

  @prop({ required: true, default: () => nanoid() })
  verificationCode: string;

  @prop()
  passwordResetCode: string | null;

  @prop({ default: false })
  verified: boolean;

  @prop({ default: "user" })
  role: string;

  @prop({ default: [] })
  cart: Array<string>;

  @prop({ ref: "Audio" })
  public wishlist?: Array<Audio>;

  async comparePassword(this: DocumentType<User>, candidatePassword: string) {
    try {
      return await argon2.verify(this.password, candidatePassword); // we verify the code's validaty with argon2
    } catch (e) {
      log.error(e, "Could not validate password"); // in case of an error password we through this error
      return false;
    }
  }
}

const UserModel = getModelForClass(User);

export { User, UserModel };
