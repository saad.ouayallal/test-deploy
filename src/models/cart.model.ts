import {
  prop,
  getModelForClass,
  Ref,
  modelOptions,
  index,
  Severity,
} from "@typegoose/typegoose";
import { User } from "./user.model";
import { Audio } from "./audio.model";

@index({ email: 1 })
@modelOptions({
  schemaOptions: {
    timestamps: true, // to add timestamps like createdAt & updatedAt
  },
  options: {
    allowMixed: Severity.ALLOW,
  },
})
class ProductInCart {
  @prop({ ref: () => Audio })
  public product!: Ref<Audio>;

  @prop()
  public price!: number;
}

export class Cart {
  @prop({ type: () => [ProductInCart] })
  public products!: ProductInCart[];

  @prop()
  public cartTotal!: number;

  //   @prop()
  //   public totalAfterDiscount!: number; maybe I'll add it after discussing it

  @prop({ ref: () => User })
  public orderby!: Ref<User>;
}

const CartModel = getModelForClass(Cart);

export default CartModel;
