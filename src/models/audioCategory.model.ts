import {
  getModelForClass,
  modelOptions,
  prop,
  Severity,
  Ref,
  index,
} from "@typegoose/typegoose";
import { ObjectId } from "mongoose";
import { User } from "./index";

@index({ email: 1 })
@modelOptions({
  schemaOptions: {
    timestamps: true, // to add timestamps like createdAt & updatedAt
  },
  options: {
    allowMixed: Severity.ALLOW,
  },
})
class Category {
  @prop({ required: true, unique: true, index: true })
  title: string;
}

const CategoryModel = getModelForClass(Category);

export { Category, CategoryModel };
