require("dotenv").config();
import express from "express";
import config from "config";
import { connectToServer } from "./db/connection";
import log from "./db/logger";
import userRouter from "./routes/user";
import swaggerDocs from "./utils/swagger";
import audioRouter from "./routes/product";
import categoryRouter from "./routes/category";
import cookieParser from "cookie-parser";
import cors from "cors";
import deserializeUser from "./middleware/deserializeUser";

const app = express();

const allowedOrigins = ["http://localhost:3000", "http://localhost:4200"];

const options: cors.CorsOptions = {
  origin: allowedOrigins,
};

app.use(cors(options));

app.use(cookieParser());
app.use(express.json());
app.use(deserializeUser);
app.use(userRouter);
app.use(audioRouter);
app.use(categoryRouter);

const port = config.get("port");

connectToServer().then(async () => {
  app.listen(port, () => {
    log.info(`Express is listening at http://localhost:${port}`);
    swaggerDocs(app, port);
  });
});
