import connectToDb from "../db/connection";
import { CategoryModel, Category } from "../models/index";

export async function createCategoryinDb(input: Partial<Category>) {
  return CategoryModel.create(input);
}
export async function updateCategoryinDb(input: Partial<Category>) {
  // return CategoryModel.updateOne(input);
}

export function findByIdAndUpdate(id: string) {
  return CategoryModel.findByIdAndUpdate(id);
}

export function findByIdAndDelete(id: string) {
  return CategoryModel.findByIdAndDelete(id);
}

export function findCategory(id: string) {
  return CategoryModel.findById(id);
}

export function findAllCategory() {
  return CategoryModel.find();
}
