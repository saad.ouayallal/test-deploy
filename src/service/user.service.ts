import connectToDb from "../db/connection";
import { omit } from "lodash";
import { User, UserModel } from "../models/index";

export async function createUser(input: Partial<User>) {
  return UserModel.create(input);
}

export function findUserById(id: string) {
  return UserModel.findById(id);
}

export function findUserByIdAndUpdate(id: string) {
  return UserModel.findByIdAndUpdate(id);
}

export function findUsers() {
  return UserModel.find();
}

export function findUser(query: any) {
  return UserModel.findOne(query).lean();
}

export async function findUserByEmail(email: string) {
  return UserModel.findOne({ email });
}

export function deleteUser(id: string) {
  return UserModel.findByIdAndDelete(id);
}

export async function validatePassword({
  email,
  password,
}: {
  email: string;
  password: string;
}) {
  const user = await UserModel.findOne({ email });

  if (!user) {
    return false;
  }

  const isValid = await user.comparePassword(password);

  if (!isValid) return false;

  return omit(user.toJSON(), "password");
}