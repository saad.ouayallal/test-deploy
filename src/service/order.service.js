"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.findOrderByTitle = exports.findAllOrders = exports.findOrderById = void 0;
const order_model_1 = __importDefault(require("../models/order.model"));
function findOrderById(id) {
    return order_model_1.default.findById(id);
}
exports.findOrderById = findOrderById;
function findAllOrders() {
    return order_model_1.default.find();
}
exports.findAllOrders = findAllOrders;
function findOrderByTitle(title) {
    return order_model_1.default.findOne({ title });
}
exports.findOrderByTitle = findOrderByTitle;
