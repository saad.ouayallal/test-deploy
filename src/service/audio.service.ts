import connectToDb from "../db/connection";
import { AudioModel, Audio } from "../models/audio.model";

export async function createAudio(input: Partial<Audio>) {
  return AudioModel.create(input);
}

export function findAudioById(id: string) {
  return AudioModel.findById(id);
}

export function findAllAudios() {
  return AudioModel.find();
}

export function findAudioByTitle(title: string) {
  return AudioModel.findOne({ title });
}
