import multer from "multer";
import { GridFsStorage } from "multer-gridfs-storage";
import dotenv from "dotenv";
dotenv.config();

const connectionString = process.env.ATLAS_URI;
if (!connectionString) {
  throw new Error("No connection string in environment variable ATLAS_URI");
}

const storage = new GridFsStorage({ url: connectionString });
const upload = multer({ storage });

export default upload;
