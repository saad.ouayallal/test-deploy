import connectToDb from "../db/connection";
import CartModel, { Cart } from "../models/cart.model";

export async function createCart(input: Partial<Cart>) {
  return CartModel.create(input);
}

export function findCartById(id: string) {
  return CartModel.findById(id);
}

export function findAllCarts() {
  return CartModel.find();
}

export function findCart(title: any) {
  return CartModel.findOne({ title });
}
