"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.findAudioByTitle = exports.findAllAudios = exports.findAudioById = exports.createAudio = void 0;
const audio_model_1 = require("../models/audio.model");
function createAudio(input) {
    return __awaiter(this, void 0, void 0, function* () {
        return audio_model_1.AudioModel.create(input);
    });
}
exports.createAudio = createAudio;
function findAudioById(id) {
    return audio_model_1.AudioModel.findById(id);
}
exports.findAudioById = findAudioById;
function findAllAudios() {
    return audio_model_1.AudioModel.find();
}
exports.findAllAudios = findAllAudios;
function findAudioByTitle(title) {
    return audio_model_1.AudioModel.findOne({ title });
}
exports.findAudioByTitle = findAudioByTitle;
