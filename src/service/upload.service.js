"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const multer_1 = __importDefault(require("multer"));
const multer_gridfs_storage_1 = require("multer-gridfs-storage");
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config();
const connectionString = process.env.ATLAS_URI;
if (!connectionString) {
    throw new Error("No connection string in environment variable ATLAS_URI");
}
const storage = new multer_gridfs_storage_1.GridFsStorage({ url: connectionString });
const upload = (0, multer_1.default)({ storage });
exports.default = upload;
