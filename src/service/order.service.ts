import connectToDb from "../db/connection";
import OrderModel from "../models/order.model";



export function findOrderById(id: string) {
  return OrderModel.findById(id);
}

export function findAllOrders() {
  return OrderModel.find();
}

export function findOrderByTitle(title: string) {
  return OrderModel.findOne({ title });
}
