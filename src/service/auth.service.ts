import { DocumentType } from "@typegoose/typegoose";
import { omit } from "lodash";
import SessionModel, { Session } from "../models/session.model";
import { privateFields, User } from "../models/user.model";
import { signJwt, verifyJwt } from "../db/jwt";
import { findUser, findUserById } from "./user.service";
import { FilterQuery, UpdateQuery } from "mongoose";
import { get } from "lodash";
import config from "config";

// export async function createSession({ userId }: { userId: string }) {
//   return SessionModel.create({ user: userId });
// }

export async function createSession(userId: string, userAgent: string) {
  const session = await SessionModel.create({ user: userId, userAgent });

  return session.toJSON();
}

export async function findSessionById(id: string) {
  return SessionModel.findById(id);
}

export async function findSessionByUser(user: string) {
  return SessionModel.findOne({ user });
}

export async function deleteSessionById(id: string) {
  return SessionModel.findByIdAndDelete(id);
}

export async function updateSession(
  query: FilterQuery<Session>,
  update: UpdateQuery<Session>
) {
  return SessionModel.updateOne(query, update);
}


export function signAccessToken(user: DocumentType<User>) {
  const payload = omit(user.toJSON(), privateFields);

  const accessToken = signJwt(payload, "accessTokenPrivateKey", {
    expiresIn: "15m",
  });

  return accessToken;
}

export async function logoutSession(sessionId: string) {
  const session = await deleteSessionById(sessionId);
  return session;
}

export async function findSessions(query: any) {
  return SessionModel.find(query).lean();
}

export async function reIssueAccessToken({
  refreshToken,
}: {
  refreshToken: string;
}) {
  const { decoded } = verifyJwt(refreshToken, "refreshTokenPublicKey");

  if (!decoded || !get(decoded, "session")) return false;

  const session = await SessionModel.findById(get(decoded, "session"));

  if (!session || !session.valid) return false;

  const user = await findUser({ _id: session.user });

  if (!user) return false;

  const accessToken = signJwt(
    { ...user, session: session._id },
    "accessTokenPrivateKey",
    { expiresIn: config.get("accessTokenTtl") } // 15 minutes
  );

  return accessToken;
}