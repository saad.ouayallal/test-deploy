import { Request, Response } from "express";
import { AudioModel, Audio } from "../models/audio.model";
import upload from "../service/upload.service";
import {
  CreateAudioInput,
  RemoveAudioInput,
  UpdateAudioInput,
  UpdateAudioParams,
} from "../schema/audio.schema";
import getDb, { getBucket } from "../db/connection";
import dotenv from "dotenv";
import { findAllAudios, findAudioById } from "../service/audio.service";
import mongoose from "mongoose";

import sendEmail from "../db/mailer";
import { findUserByEmail, findUserById } from "../service/user.service";
import log from "../db/logger";
import { ObjectId } from "mongodb";
import { VerifyUserInput } from "../schema/user.schema";
import { UserModel } from "../models";
import { findCategory } from "../service/category.service";

dotenv.config();

const db = getDb();
const collection = db.collection<Audio>("audios");

// Controller methods for CRUD operations using GridFS and Typegoose
// export async function createAudioHandler(
//   req: Request,
//   res: Response
// ) {
//   try {
//     upload.single("audio")(req, res, async (err) => {
//       const userId = req.params.id;
//       if (!req.file?.filename) {
//         res.status(400).send("File not uploaded");
//         return;
//       }

//       const newAudio = new AudioModel({
//         filename: req.file.filename,
//         mimetype: req.file.mimetype,
//         title: req.body.title,
//         duration: req.body.duration,
//         category: req.body.category,
//         description: req.body.description,
//         price: req.body.price,
//         images: req.body.images,
//         ratings: { stars: 5, postedby: userId },
//         uploaded: new Date(),
//       });

//       const result = await collection.insertOne({
//         filename: req.file.filename,
//         mimetype: req.file.mimetype,
//         title: req.body.title,
//         duration: req.body.duration,
//         category: req.body.category,
//         description: req.body.description,
//         price: req.body.price,
//         images: req.body.images,
//         ratings: { stars: 5, postedby: userId },
//         uploaded: new Date(),
//       });

//       await sendEmail({
//         to: "saad.ouayallal@gmail.com",
//         from: "test@example.com",
//         subject: "Adding audio to Library",
//         text: `Audio ID : ${result.insertedId}`,
//       });

//       return res.status(200).json(result);
//     });
//   } catch (error) {
//     return res.status(404).json({ error: "Internal error" });
//   }
// }

export async function createAudioHandler(req: Request, res: Response) {
  try {
    upload.single("audio")(req, res, async (err: any) => {
      const userId = new ObjectId(req.params.id);
      if (!req.file?.filename) {
        res.status(400).send({ errorMessage: "File not uploaded" });
        return;
      }

      const newAudio = new AudioModel({
        filename: req?.file?.filename,
        mimetype: req?.file?.mimetype,
        title: req.body.title,
        duration: req.body.duration,
        description: req.body.description,
        price: req.body.price,
        uploaded: new Date(),
      });

      try {
        const result = await newAudio.save();

        return res.status(200).send({ id: result.id, result });
      } catch (error: any) {
        return res.status(500).json({ errorMessage: error.message });
      }
    });
  } catch (error) {
    return res.status(500).json({ errorMessage: "Internal error" });
  }
}

export async function getAudioHandler(
  req: Request<VerifyUserInput>,
  res: Response
) {
  try {
    const id = req.params.id;
    const bucket = getBucket();
    const audio = await findAudioById(id);

    if (!audio) {
      return res.status(404).json({ errorMessage: "Audio not found" });
    }

    res.status(200);

    res.set({
      "Content-Type": audio.mimetype,
      "Transfer-Encoding": "chunked",
    });
    // Create a readable stream from GridFS using the audio's filename
    bucket.openDownloadStreamByName(audio.filename).pipe(res);
  } catch (error) {
    return res.status(500).json({ errorMessage: "Internal Error" });
  }
}

export async function getAllAudiosHandler(
  req: Request<VerifyUserInput>,
  res: Response
) {
  try {
    // Filtering the data
    const queryObj = { ...req.query };
    const excludeFileds = ["filename", "title", "description"];
    excludeFileds.forEach((el) => {
      delete queryObj[el];
    });
    let queryStr = JSON.stringify(queryObj);
    queryStr = queryStr.replace(/\b(gte|gt|lte|lt)\b/g, (match) => `$${match}`);

    // gte = greater than or equal to
    // gt = greater than
    // lte = lower than or equal to
    // lt = lower than

    let query = AudioModel.find(JSON.parse(queryStr));

    const audios = await query;

    console.log(audios);
    if (!audios) {
      return res.status(404).json({ errorMessage: "Audios not found" });
    }

    return res.status(200).send({ audios });
  } catch (error) {
    return res.status(500).send({ errorMessage: "Internal error" });
  }
}

export async function updateAudioHandler(
  req: Request<UpdateAudioParams>,
  res: Response
) {
  try {
    const { email } = req.body;
    const user = await findUserByEmail(email);

    if (!user) {
      return res
        .status(404)
        .send({ errorMessage: `User with email ${email} does not exists` });
    }

    if (!user.verified) {
      return res.status(401).send({ errorMessage: "User is not verified" });
    }

    const audioId = req.params.id;
    const { name } = req.body;

    const audio = await AudioModel.findByIdAndUpdate(
      audioId,
      { name },
      { new: true }
    );

    if (!audio) {
      return res.status(404).send({ errorMessage: "Audio not found" });
    }

    return res.status(200).json(audio);
  } catch (error) {
    return res.status(500).send({ errorMessage: "Internal server error" });
  }
}

export async function removeAudioHandler(req: Request, res: Response) {
  const id = req.params.id;
  const bucket = getBucket();

  try {
    const audio = await findAudioById(id);

    if (!audio) {
      return res.status(404).send({ errorMessage: "Audio not found" });
    }
    const filename = audio.filename;

    // // Remove the file from GridFS bucket
    const audioFiles = await bucket
      .find({
        filename,
      })
      .toArray();

    if (!audioFiles) {
      return res
        .status(404)
        .send({ errorMessage: "File not found or already deleted" });
    }

    Promise.all(
      audioFiles.map((audioFile) => {
        bucket.delete(audioFile._id);
      })
    );

    // Remove the audio document from the collection
    await AudioModel.findByIdAndRemove(id);

    return res
      .status(200)
      .send({ code: 200, errorMessage: "Audio Successfully Removed" });
  } catch (error) {
    return res.status(500).json({ errorMessage: "Internal server error" });
  }
}

export async function addToWishlist(req: Request, res: Response) {
  const id = req.body.id;
  const audioId = req.body.audioId;

  try {
    const user = await findUserById(id);

    console.log(user?.wishlist);
    const alreadyAdded = user?.wishlist?.find(
      (id) => id.toString() === audioId
    );
    console.log(alreadyAdded);
    let updatedUser;

    if (alreadyAdded) {
      updatedUser = await UserModel.findByIdAndUpdate(
        id,
        { $pull: { wishlist: audioId } },
        { new: true }
      );
    } else {
      updatedUser = await UserModel.findByIdAndUpdate(
        id,
        { $push: { wishlist: audioId } },
        { new: true }
      );
    }

    res.status(200).json(updatedUser);
  } catch (error: any) {
    return res.status(500).json({ error: "Internal server error" });
  }
}

export async function addCategoryToAudio(req: Request, res: Response) {
  const categoryId = req.body.categoryId;
  const audioId = req.body.audioId;

  try {
    const audio = await findAudioById(audioId);
    console.log(audio?.category);
    const alreadyAdded = audio?.category?.find(
      (category) => category.toString() === categoryId
    );

    console.log(alreadyAdded);
    let updatedAudio;

    if (alreadyAdded) {
      updatedAudio = await AudioModel.findByIdAndUpdate(
        audioId,
        { $pull: { category: categoryId } },
        { new: true }
      );
    } else {
      updatedAudio = await AudioModel.findByIdAndUpdate(
        audioId,
        { $push: { category: categoryId } },
        { new: true }
      );
    }

    res.status(200).json(updatedAudio);
  } catch (error: any) {
    return res.status(500).json({ error: "Internal server error" });
  }
}

// To add put function We need to discuss it with AlifStory
