"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.addCategoryToAudio = exports.addToWishlist = exports.removeAudioHandler = exports.updateAudioHandler = exports.getAllAudiosHandler = exports.getAudioHandler = exports.createAudioHandler = void 0;
const audio_model_1 = require("../models/audio.model");
const upload_service_1 = __importDefault(require("../service/upload.service"));
const connection_1 = __importStar(require("../db/connection"));
const dotenv_1 = __importDefault(require("dotenv"));
const audio_service_1 = require("../service/audio.service");
const user_service_1 = require("../service/user.service");
const mongodb_1 = require("mongodb");
const models_1 = require("../models");
dotenv_1.default.config();
const db = (0, connection_1.default)();
const collection = db.collection("audios");
// Controller methods for CRUD operations using GridFS and Typegoose
// export async function createAudioHandler(
//   req: Request,
//   res: Response
// ) {
//   try {
//     upload.single("audio")(req, res, async (err) => {
//       const userId = req.params.id;
//       if (!req.file?.filename) {
//         res.status(400).send("File not uploaded");
//         return;
//       }
//       const newAudio = new AudioModel({
//         filename: req.file.filename,
//         mimetype: req.file.mimetype,
//         title: req.body.title,
//         duration: req.body.duration,
//         category: req.body.category,
//         description: req.body.description,
//         price: req.body.price,
//         images: req.body.images,
//         ratings: { stars: 5, postedby: userId },
//         uploaded: new Date(),
//       });
//       const result = await collection.insertOne({
//         filename: req.file.filename,
//         mimetype: req.file.mimetype,
//         title: req.body.title,
//         duration: req.body.duration,
//         category: req.body.category,
//         description: req.body.description,
//         price: req.body.price,
//         images: req.body.images,
//         ratings: { stars: 5, postedby: userId },
//         uploaded: new Date(),
//       });
//       await sendEmail({
//         to: "saad.ouayallal@gmail.com",
//         from: "test@example.com",
//         subject: "Adding audio to Library",
//         text: `Audio ID : ${result.insertedId}`,
//       });
//       return res.status(200).json(result);
//     });
//   } catch (error) {
//     return res.status(404).json({ error: "Internal error" });
//   }
// }
function createAudioHandler(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            upload_service_1.default.single("audio")(req, res, (err) => __awaiter(this, void 0, void 0, function* () {
                var _a, _b, _c;
                const userId = new mongodb_1.ObjectId(req.params.id);
                if (!((_a = req.file) === null || _a === void 0 ? void 0 : _a.filename)) {
                    res.status(400).send({ errorMessage: "File not uploaded" });
                    return;
                }
                const newAudio = new audio_model_1.AudioModel({
                    filename: (_b = req === null || req === void 0 ? void 0 : req.file) === null || _b === void 0 ? void 0 : _b.filename,
                    mimetype: (_c = req === null || req === void 0 ? void 0 : req.file) === null || _c === void 0 ? void 0 : _c.mimetype,
                    title: req.body.title,
                    duration: req.body.duration,
                    description: req.body.description,
                    price: req.body.price,
                    uploaded: new Date(),
                });
                try {
                    const result = yield newAudio.save();
                    return res.status(200).send({ id: result.id, result });
                }
                catch (error) {
                    return res.status(500).json({ errorMessage: error.message });
                }
            }));
        }
        catch (error) {
            return res.status(500).json({ errorMessage: "Internal error" });
        }
    });
}
exports.createAudioHandler = createAudioHandler;
function getAudioHandler(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const id = req.params.id;
            const bucket = (0, connection_1.getBucket)();
            const audio = yield (0, audio_service_1.findAudioById)(id);
            if (!audio) {
                return res.status(404).json({ errorMessage: "Audio not found" });
            }
            res.status(200);
            res.set({
                "Content-Type": audio.mimetype,
                "Transfer-Encoding": "chunked",
            });
            // Create a readable stream from GridFS using the audio's filename
            bucket.openDownloadStreamByName(audio.filename).pipe(res);
        }
        catch (error) {
            return res.status(500).json({ errorMessage: "Internal Error" });
        }
    });
}
exports.getAudioHandler = getAudioHandler;
function getAllAudiosHandler(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            // Filtering the data
            const queryObj = Object.assign({}, req.query);
            const excludeFileds = ["filename", "title", "description"];
            excludeFileds.forEach((el) => {
                delete queryObj[el];
            });
            let queryStr = JSON.stringify(queryObj);
            queryStr = queryStr.replace(/\b(gte|gt|lte|lt)\b/g, (match) => `$${match}`);
            // gte = greater than or equal to
            // gt = greater than
            // lte = lower than or equal to
            // lt = lower than
            let query = audio_model_1.AudioModel.find(JSON.parse(queryStr));
            const audios = yield query;
            console.log(audios);
            if (!audios) {
                return res.status(404).json({ errorMessage: "Audios not found" });
            }
            return res.status(200).send({ audios });
        }
        catch (error) {
            return res.status(500).send({ errorMessage: "Internal error" });
        }
    });
}
exports.getAllAudiosHandler = getAllAudiosHandler;
function updateAudioHandler(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const { email } = req.body;
            const user = yield (0, user_service_1.findUserByEmail)(email);
            if (!user) {
                return res
                    .status(404)
                    .send({ errorMessage: `User with email ${email} does not exists` });
            }
            if (!user.verified) {
                return res.status(401).send({ errorMessage: "User is not verified" });
            }
            const audioId = req.params.id;
            const { name } = req.body;
            const audio = yield audio_model_1.AudioModel.findByIdAndUpdate(audioId, { name }, { new: true });
            if (!audio) {
                return res.status(404).send({ errorMessage: "Audio not found" });
            }
            return res.status(200).json(audio);
        }
        catch (error) {
            return res.status(500).send({ errorMessage: "Internal server error" });
        }
    });
}
exports.updateAudioHandler = updateAudioHandler;
function removeAudioHandler(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const id = req.params.id;
        const bucket = (0, connection_1.getBucket)();
        try {
            const audio = yield (0, audio_service_1.findAudioById)(id);
            if (!audio) {
                return res.status(404).send({ errorMessage: "Audio not found" });
            }
            const filename = audio.filename;
            // // Remove the file from GridFS bucket
            const audioFiles = yield bucket
                .find({
                filename,
            })
                .toArray();
            if (!audioFiles) {
                return res
                    .status(404)
                    .send({ errorMessage: "File not found or already deleted" });
            }
            Promise.all(audioFiles.map((audioFile) => {
                bucket.delete(audioFile._id);
            }));
            // Remove the audio document from the collection
            yield audio_model_1.AudioModel.findByIdAndRemove(id);
            return res
                .status(200)
                .send({ code: 200, errorMessage: "Audio Successfully Removed" });
        }
        catch (error) {
            return res.status(500).json({ errorMessage: "Internal server error" });
        }
    });
}
exports.removeAudioHandler = removeAudioHandler;
function addToWishlist(req, res) {
    var _a;
    return __awaiter(this, void 0, void 0, function* () {
        const id = req.body.id;
        const audioId = req.body.audioId;
        try {
            const user = yield (0, user_service_1.findUserById)(id);
            console.log(user === null || user === void 0 ? void 0 : user.wishlist);
            const alreadyAdded = (_a = user === null || user === void 0 ? void 0 : user.wishlist) === null || _a === void 0 ? void 0 : _a.find((id) => id.toString() === audioId);
            console.log(alreadyAdded);
            let updatedUser;
            if (alreadyAdded) {
                updatedUser = yield models_1.UserModel.findByIdAndUpdate(id, { $pull: { wishlist: audioId } }, { new: true });
            }
            else {
                updatedUser = yield models_1.UserModel.findByIdAndUpdate(id, { $push: { wishlist: audioId } }, { new: true });
            }
            res.status(200).json(updatedUser);
        }
        catch (error) {
            return res.status(500).json({ error: "Internal server error" });
        }
    });
}
exports.addToWishlist = addToWishlist;
function addCategoryToAudio(req, res) {
    var _a;
    return __awaiter(this, void 0, void 0, function* () {
        const categoryId = req.body.categoryId;
        const audioId = req.body.audioId;
        try {
            const audio = yield (0, audio_service_1.findAudioById)(audioId);
            console.log(audio === null || audio === void 0 ? void 0 : audio.category);
            const alreadyAdded = (_a = audio === null || audio === void 0 ? void 0 : audio.category) === null || _a === void 0 ? void 0 : _a.find((category) => category.toString() === categoryId);
            console.log(alreadyAdded);
            let updatedAudio;
            if (alreadyAdded) {
                updatedAudio = yield audio_model_1.AudioModel.findByIdAndUpdate(audioId, { $pull: { category: categoryId } }, { new: true });
            }
            else {
                updatedAudio = yield audio_model_1.AudioModel.findByIdAndUpdate(audioId, { $push: { category: categoryId } }, { new: true });
            }
            res.status(200).json(updatedAudio);
        }
        catch (error) {
            return res.status(500).json({ error: "Internal server error" });
        }
    });
}
exports.addCategoryToAudio = addCategoryToAudio;
// To add put function We need to discuss it with AlifStory
