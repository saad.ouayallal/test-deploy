import { Request, Response } from "express";
import { CreateSessionInput } from "../schema/auth.schema";
import {
  createSession,
  findSessions,
  updateSession,
} from "../service/auth.service";
import { findUserByEmail, validatePassword } from "../service/user.service";
import { signJwt } from "../db/jwt";
import config from "config";

export async function createUserSessionHandler(
  req: Request<{}, {}, CreateSessionInput>,
  res: Response
) {
  try {
    const message = "Invalid email or password";

    const user = await validatePassword(req.body);

    if (!user) {
      return res.status(400).send({ errorMessage: message });
    }

    // Create a session
    const session = await createSession(
      user._id.toString(),
      req.get("user-agent") || ""
    );

    // create an access token
    const accessToken = signJwt(
      { ...user, session: session._id },
      "accessTokenPrivateKey",
      { expiresIn: config.get("accessTokenTtl") } // 15 minutes,
    );

    // create a refresh token
    const refreshToken = signJwt(
      { ...user, session: session._id },
      "refreshTokenPrivateKey",
      { expiresIn: config.get("refreshTokenTtl") } // 15 minutes
    );

    // Set cookies
    res.cookie("accessToken", accessToken, {
      httpOnly: true,
      secure: true, // only set this cookie on HTTPS
      maxAge: 900000, // Convert expiresIn to milliseconds
    });

    res.cookie("refreshToken", refreshToken, {
      httpOnly: true,
      secure: true, // only set this cookie on HTTPS
      maxAge: 900000, // Convert expiresIn to milliseconds
    });

    // return access & refresh tokens
    return res
      .status(200)
      .send({ code: 200, message: { accessToken, refreshToken } });
  } catch (error) {
    return res.status(500).send({
      errorMessage: "Internal error",
    });
  }
}

export async function createAdminSessionHandler(
  req: Request<{}, {}, CreateSessionInput>,
  res: Response
) {
  try {
    const message = "Invalid email or password";

    const user = await validatePassword(req.body);

    if (!user) {
      return res.status(400).send({ errorMessage: message });
    }

    if (user.role !== "admin") {
      return res.status(400).send({ errorMessage: message });
    }

    // Create a session
    const session = await createSession(
      user._id.toString(),
      req.get("user-agent") || ""
    );

    // create an access token
    const accessToken = signJwt(
      { ...user, session: session._id },
      "accessTokenPrivateKey",
      { expiresIn: config.get("accessTokenTtl") } // 15 minutes,
    );

    // create a refresh token
    const refreshToken = signJwt(
      { ...user, session: session._id },
      "refreshTokenPrivateKey",
      { expiresIn: config.get("refreshTokenTtl") } // 15 minutes
    );

    // Set cookies
    res.cookie("accessToken", accessToken, {
      httpOnly: true,
      secure: true, // only set this cookie on HTTPS
      maxAge: 900000, // Convert expiresIn to milliseconds
    });

    res.cookie("refreshToken", refreshToken, {
      httpOnly: true,
      secure: true, // only set this cookie on HTTPS
      maxAge: 900000, // Convert expiresIn to milliseconds
    });

    // return access & refresh tokens
    return res
      .status(200)
      .send({ code: 200, message: { accessToken, refreshToken } });
  } catch (error) {
    return res.status(500).send({
      errorMessage: "Internal error",
    });
  }
}

export async function signOutHandler(req: Request, res: Response) {
  try {
    const sessionId = res.locals.user.session;
    await updateSession({ _id: sessionId }, { valid: false });

    return res.status(200).send({
      code: 200,
      message: "User Successfully logged out",
      accessToken: null,
      refreshToken: null,
    });
  } catch (error) {
    return res.status(500).send({
      errorMessage: "Internal error",
    });
  }
}

export async function getUserSessionsHandler(req: Request, res: Response) {
  const userId = res.locals.user._id;

  const sessions = await findSessions({ user: userId, valid: true });

  return res.send(sessions);
}
