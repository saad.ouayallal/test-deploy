import { Request, Response } from "express";
import { Category } from "../models/index";
import {
  createCategoryinDb,
  findAllCategory,
  findByIdAndDelete,
  findByIdAndUpdate,
  findCategory,
} from "../service/category.service";
import validateMongoDbId from "../utils/validateMongoDBId";

//
export async function createCategory(req: Request, res: Response) {
  try {
    const newCategory = await createCategoryinDb(req.body);
    res.status(200).json({ code: 200, result: newCategory });
  } catch (error: any) {
    return res.status(500).send({ errorMessage : error.message });
  }
}

export async function updateCategory(req: Request, res: Response) {
  const { id } = req.params;
  validateMongoDbId(id);
  try {
    const updatedCategory = await findByIdAndUpdate(id);
    res.status(200).json({ code: 200, result: updatedCategory });
  } catch (error: any) {
    return res.status(500).send({ errorMessage: error.message });
  }
}

export async function deleteCategory(req: Request, res: Response) {
  const { id } = req.params;
  validateMongoDbId(id);
  try {
    const deletedCategory = await findByIdAndDelete(id);
    if(!deleteCategory){
      return res.status(404).json({ errorMessage: `Category not found` }); 
    } else return res.status(200).json({ code: 200, message: `Category ${deletedCategory?.title} deleted successfuly` }); 
  } catch (error: any) {
    return res.status(500).send({ errorMessage: error.message });
  }
}

//
export async function getCategory(req: Request, res: Response) {
  const { id } = req.params;
  validateMongoDbId(id);
  try {
    const getaCategory = await findCategory(id);
    res.status(200).json({ code: 200, result: getaCategory });
  } catch (error: any) {
    return res.status(500).send({ errorMessage: error.message });
  }
}
//
export async function getallCategory(req: Request, res: Response) {
  try {
    const getallCategory = await findAllCategory();
    res.status(200).send({ code: 200, result: getallCategory });
  } catch (error: any) {
    return res.status(500).send({ errorMessage: error.message });
  }
}
