import { Request, Response } from "express";
import {
  CreateUserInput,
  ForgotPasswordInput,
  ResetPasswordInput,
  VerifyUserInput,
} from "../schema/user.schema";
import {
  createUser,
  findUserByEmail,
  findUserById,
  findUsers,
  deleteUser,
} from "../service/user.service";
import sendEmail from "../db/mailer";
import log from "../db/logger";
import { nanoid } from "nanoid";
import { findCart } from "../service/cart.service";
import validateMongoDbId from "../utils/validateMongoDBId";
import { findAudioById } from "../service/audio.service";
import CartModel from "../models/cart.model";
import OrderModel from "../models/order.model";
import { errorMonitor } from "events";
import orderModel from "../models/order.model";
import { findAllOrders } from "../service/order.service";
import { verifyJwt } from "../db/jwt";

export async function createUserHandler(
  req: Request<{}, {}, CreateUserInput>,
  res: Response
) {
  const body = req.body;

  try {
    const user = await createUser(body);
    await sendEmail({
      to: user.email,
      from: "diginervaconsulting@gmail.com",
      subject: "Verify your email",
      text: `verification code: ${user.verificationCode} Id: ${user._id}`,
    });

    console.log(user.verificationCode, user._id);
    return res.status(200).send({code : 200, message: "User successfully created"});
  } catch (e: any) {
    if (e.code === 11000) {
      return res.status(409).send({
        errorMessage: "Account already exists",
      });
    }

    return res.status(500).send({
      errorMessage: "Internal server error",
    });
  }
}
export async function deleteUserHandler(
  req: Request<VerifyUserInput>,
  res: Response
) {
  const id = req.params.id;

  try {
    const user = await deleteUser(id);

    return res.status(200).send({
      code: 200,
      message: `User has been deleted`,
    });
  } catch (e: any) {
    return res.status(500).send({ errorMessage: e });
  }
}

export async function verifyUserHandler(
  req: Request<VerifyUserInput>,
  res: Response
) {
  const id = req.params.id;
  const verificationCode = req.params.verificationCode;
  // find the user by id
  try {
    const user = await findUserById(id);

    //code & message init
    let code;
    let errorMessage;
    if (user) {
      if (user.verified) {
        code = 400;
        errorMessage = "User is already verified";
      } else if (user.verificationCode === verificationCode) {
        user.verified = true;
        await user.save();
        return res
          .status(200)
          .send({ code: 200, message: "User successfully verified" });
      } else {
        code = 400;
        errorMessage = "Verification code does not match";
      }
    } else {
      code = 404;
      errorMessage = "User not found";
    }

    return res.status(code).send({ errorMessage });
  } catch (err: any) {
    if (err?.reason) {
      return res.status(404).send({
        errorMessage:
          "ID passed in must be a string of 12 bytes or a string of 24 hex characters or an integer",
      });
    } else
      return res.status(500).send({ errorMessage: "Internal server error" });
  }
}

// I need to Add the update User & Block-user & unBlock-user

export async function turnUsertoAdminHandler(
  req: Request<VerifyUserInput>,
  res: Response
) {
  const id = req.params.id;
  // find the user by id
  try {
    const user = await findUserById(id);

    if (!user) {
      return res.status(400).send({ errorMessage: "Could not verify user" });
    }
    // check to see if they are already an Admin
    if (user.role === 'admin') {
      return res.status(401).send({
        errorMessage: "User is Already has the privilige of an Admin",
      });
    }

    // check to see if the verificationCode matches
    if (user.verified) {
      user.role = "admin";
      await user.save();
      return res.status(200).send({
        code: 200,
        message: "User successfully turned to Admin",
      });
    }
  } catch (err) {
    return res
      .status(500)
      .send({ errorMessage: "Could not give the priviliges of an admin" });
  }
}

export async function forgotPasswordHandler(
  req: Request<{}, {}, ForgotPasswordInput>,
  res: Response
) {
  try {
    const { email } = req.body;

    const user = await findUserByEmail(email);

    if (!user) {
      return res.status(404).send({
        errorMessage: `User with email ${email} does not exist`,
      });
    }

    if (!user.verified) {
      return res.status(400).send({
        errorMessage: "User is not verified",
      });
    }

    const passwordResetCode = nanoid();

    user.passwordResetCode = passwordResetCode;

    await user.save();

    await sendEmail({
      to: user.email,
      from: "test@example.com",
      subject: "Reset your password",
      text: `Password reset code: ${passwordResetCode}. Id ${user._id}`,
    });

    return res.status(200).send({
      code: 200,
      message: `Password reset email sent to ${email}`,
    });
  } catch (error) {
    return res.status(500).send({
      errorMessage: "Internal error",
    });
  }
}

export async function resetPasswordHandler(
  req: Request<ResetPasswordInput["params"], {}, ResetPasswordInput["body"]>,
  res: Response
) {
  try {
    const { id, passwordResetCode } = req.params;

    const { password } = req.body;

    const user = await findUserById(id);

    if (
      !user ||
      !user.passwordResetCode ||
      user.passwordResetCode !== passwordResetCode
    ) {
      return res.status(400).send({
        errorMessage: "Could not reset user password",
      });
    }

    user.passwordResetCode = null;

    user.password = password;

    await user.save();
    return res.status(200).send({
      code: 200,
      message: "Successfully updated password",
    });
  } catch (err) {
    return res.status(500).send({
      errorMessage: "Internal server error",
    });
  }
}

export async function getCurrentUserHandler(req: Request, res: Response) {
  return res.status(200).send({code:  200, message: res.locals.user});
}

export async function getAllUsersHandler(req: Request, res: Response) {
  try {
    const users = await findUsers();
    return res.send(users);
  } catch (error) {
    return res.status(500).send(error);
  }
}

export async function getWishlistHandler(req: Request, res: Response) {
  const id = req.params.id;
  try {
    const wishlist = await findUserById(id).populate("wishlist");
    return res.status(200).send(wishlist?.wishlist);
  } catch (error) {
    return res.status(500).send({ errorMessage: "Internal error" });
  }
}

export async function userCart(req: Request, res: Response) {
  const { cart } = req.body;
  const id = req.params.id;
  try {
    validateMongoDbId(id);
    let products = [];
    const user = await findUserById(id);
    // check if user already have product in cart

    const alreadyExistCart = await CartModel.findOne({ orderby: user?.id });
    if (alreadyExistCart) {
      await CartModel.deleteOne({ _id: alreadyExistCart._id });
    }
    for (let i = 0; i < cart.length; i++) {
      let getPrice = await findAudioById(cart[i].id).select("price").exec();
      let object = {
        product: cart[i].id,
        price: getPrice!.price,
      };
      products.push(object);
    }
    let cartTotal = 0;
    for (let i = 0; i < products.length; i++) {
      cartTotal = cartTotal + products[i].price;
    }
    let newCart = await new CartModel({
      products,
      cartTotal,
      orderby: user?.id,
    }).save();
    res.status(200).send(newCart);
  } catch (error: any) {
    return res.status(500).send({ errorMessage: "Internal error" });
  }
}

export async function getUserCart(req: Request, res: Response) {
  const id = req.params.id;
  validateMongoDbId(id);
  try {
    const cart = await CartModel.findOne({ orderby: id }).populate(
      "products.product"
    );
    res.status(200).send(cart);
  } catch (error) {
    return res.status(500).send({ errorMessage: "Internal error" });
  }
}

export async function emptyUserCart(req: Request, res: Response) {
  const id = req.params.id;
  validateMongoDbId(id);
  try {
    const cart = await CartModel.findOneAndRemove({ orderby: id });
    res.status(200).json(cart);
  } catch (error) {
    return res.status(500).send(error);
  }
}

export async function createOrder(req: Request, res: Response) {
  const id = req.params.id;
  const { PO } = req.body;
  validateMongoDbId(id);
  try {
    if (!PO) throw new Error("Create online payment failed");
    let user = await findUserById(id);
    let userCart = await CartModel.findOne({ orderby: id });
    if (!userCart) {
      return res.json({ message: "Cart is empty" });
    }
    console.log(userCart.products.length);
    if (userCart.products.length === 0) {
      return res.json({ message: "Please insert a product into cart" });
    }
    let finalAmout = userCart?.cartTotal;
    // maybe I'll add coupon logic here

    let newOrder = await new OrderModel({
      products: userCart?.products,
      paymentIntent: {
        id: nanoid(),
        method: "PO",
        amount: finalAmout,
        status: "Payment Online",
        created: Date.now(),
        currency: "usd", // maybe we'll change this to suit the client's country
      },
      orderby: user?._id,
      orderStatus: "Payment Online",
    }).save();
    res.status(200).json({ message: newOrder });
  } catch (error) {
    return res.status(500).send(error);
  }
}

export async function getOrderByUserId(req: Request, res: Response) {
  const id = req.params.id;
  validateMongoDbId(id);
  try {
    const userOrder = await OrderModel.findOne({ orderby: id })
      .populate("products.product")
      .populate("orderby")
      .exec();
    if (!userOrder) {
      return res.json({ message: "Theres no order created by this user" });
    }
    res.status(200).send(userOrder);
  } catch (error) {
    return res.status(500).send({ errorMessage: "Internal error" });
  }
}

export async function getAllOrders(req: Request, res: Response) {
  try {
    const userOrders = await findAllOrders()
      .populate("products.product")
      .populate("orderby")
      .exec();
    if (!userOrders) {
      return res.json({ message: "Theres no orders" });
    }
    res.status(200).send(userOrders);
  } catch (error) {
    return res.status(500).send({ errorMessage: "Internal error" });
  }
}

export async function UpdateOrderStatus(req: Request, res: Response) {
  const { status } = req.body;
  const orderId = req.params.id;
  try {
    const updateOrderStatus = await OrderModel.findByIdAndUpdate(
      orderId,
      {
        orderStatus: status,
        paymentIntent: {
          status: status,
        },
      },
      { new: true }
    );
    res.status(200).send(updateOrderStatus);
  } catch (error) {
    return res.status(500).send({ errorMessage: "Internal error" });
  }
}

// I'll add in the Future Save Adresse
