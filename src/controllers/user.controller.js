"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UpdateOrderStatus = exports.getAllOrders = exports.getOrderByUserId = exports.createOrder = exports.emptyUserCart = exports.getUserCart = exports.userCart = exports.getWishlistHandler = exports.getAllUsersHandler = exports.getCurrentUserHandler = exports.resetPasswordHandler = exports.forgotPasswordHandler = exports.turnUsertoAdminHandler = exports.verifyUserHandler = exports.deleteUserHandler = exports.createUserHandler = void 0;
const user_service_1 = require("../service/user.service");
const mailer_1 = __importDefault(require("../db/mailer"));
const nanoid_1 = require("nanoid");
const validateMongoDBId_1 = __importDefault(require("../utils/validateMongoDBId"));
const audio_service_1 = require("../service/audio.service");
const cart_model_1 = __importDefault(require("../models/cart.model"));
const order_model_1 = __importDefault(require("../models/order.model"));
const order_service_1 = require("../service/order.service");
function createUserHandler(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const body = req.body;
        try {
            const user = yield (0, user_service_1.createUser)(body);
            yield (0, mailer_1.default)({
                to: user.email,
                from: "diginervaconsulting@gmail.com",
                subject: "Verify your email",
                text: `verification code: ${user.verificationCode} Id: ${user._id}`,
            });
            console.log(user.verificationCode, user._id);
            return res.status(200).send({ code: 200, message: "User successfully created" });
        }
        catch (e) {
            if (e.code === 11000) {
                return res.status(409).send({
                    errorMessage: "Account already exists",
                });
            }
            return res.status(500).send({
                errorMessage: "Internal server error",
            });
        }
    });
}
exports.createUserHandler = createUserHandler;
function deleteUserHandler(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const id = req.params.id;
        try {
            const user = yield (0, user_service_1.deleteUser)(id);
            return res.status(200).send({
                code: 200,
                message: `User has been deleted`,
            });
        }
        catch (e) {
            return res.status(500).send({ errorMessage: e });
        }
    });
}
exports.deleteUserHandler = deleteUserHandler;
function verifyUserHandler(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const id = req.params.id;
        const verificationCode = req.params.verificationCode;
        // find the user by id
        try {
            const user = yield (0, user_service_1.findUserById)(id);
            //code & message init
            let code;
            let errorMessage;
            if (user) {
                if (user.verified) {
                    code = 400;
                    errorMessage = "User is already verified";
                }
                else if (user.verificationCode === verificationCode) {
                    user.verified = true;
                    yield user.save();
                    return res
                        .status(200)
                        .send({ code: 200, message: "User successfully verified" });
                }
                else {
                    code = 400;
                    errorMessage = "Verification code does not match";
                }
            }
            else {
                code = 404;
                errorMessage = "User not found";
            }
            return res.status(code).send({ errorMessage });
        }
        catch (err) {
            if (err === null || err === void 0 ? void 0 : err.reason) {
                return res.status(404).send({
                    errorMessage: "ID passed in must be a string of 12 bytes or a string of 24 hex characters or an integer",
                });
            }
            else
                return res.status(500).send({ errorMessage: "Internal server error" });
        }
    });
}
exports.verifyUserHandler = verifyUserHandler;
// I need to Add the update User & Block-user & unBlock-user
function turnUsertoAdminHandler(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const id = req.params.id;
        // find the user by id
        try {
            const user = yield (0, user_service_1.findUserById)(id);
            if (!user) {
                return res.status(400).send({ errorMessage: "Could not verify user" });
            }
            // check to see if they are already an Admin
            if (user.role === 'admin') {
                return res.status(401).send({
                    errorMessage: "User is Already has the privilige of an Admin",
                });
            }
            // check to see if the verificationCode matches
            if (user.verified) {
                user.role = "admin";
                yield user.save();
                return res.status(200).send({
                    code: 200,
                    message: "User successfully turned to Admin",
                });
            }
        }
        catch (err) {
            return res
                .status(500)
                .send({ errorMessage: "Could not give the priviliges of an admin" });
        }
    });
}
exports.turnUsertoAdminHandler = turnUsertoAdminHandler;
function forgotPasswordHandler(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const { email } = req.body;
            const user = yield (0, user_service_1.findUserByEmail)(email);
            if (!user) {
                return res.status(404).send({
                    errorMessage: `User with email ${email} does not exist`,
                });
            }
            if (!user.verified) {
                return res.status(400).send({
                    errorMessage: "User is not verified",
                });
            }
            const passwordResetCode = (0, nanoid_1.nanoid)();
            user.passwordResetCode = passwordResetCode;
            yield user.save();
            yield (0, mailer_1.default)({
                to: user.email,
                from: "test@example.com",
                subject: "Reset your password",
                text: `Password reset code: ${passwordResetCode}. Id ${user._id}`,
            });
            return res.status(200).send({
                code: 200,
                message: `Password reset email sent to ${email}`,
            });
        }
        catch (error) {
            return res.status(500).send({
                errorMessage: "Internal error",
            });
        }
    });
}
exports.forgotPasswordHandler = forgotPasswordHandler;
function resetPasswordHandler(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const { id, passwordResetCode } = req.params;
            const { password } = req.body;
            const user = yield (0, user_service_1.findUserById)(id);
            if (!user ||
                !user.passwordResetCode ||
                user.passwordResetCode !== passwordResetCode) {
                return res.status(400).send({
                    errorMessage: "Could not reset user password",
                });
            }
            user.passwordResetCode = null;
            user.password = password;
            yield user.save();
            return res.status(200).send({
                code: 200,
                message: "Successfully updated password",
            });
        }
        catch (err) {
            return res.status(500).send({
                errorMessage: "Internal server error",
            });
        }
    });
}
exports.resetPasswordHandler = resetPasswordHandler;
function getCurrentUserHandler(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        return res.status(200).send({ code: 200, message: res.locals.user });
    });
}
exports.getCurrentUserHandler = getCurrentUserHandler;
function getAllUsersHandler(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const users = yield (0, user_service_1.findUsers)();
            return res.send(users);
        }
        catch (error) {
            return res.status(500).send(error);
        }
    });
}
exports.getAllUsersHandler = getAllUsersHandler;
function getWishlistHandler(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const id = req.params.id;
        try {
            const wishlist = yield (0, user_service_1.findUserById)(id).populate("wishlist");
            return res.status(200).send(wishlist === null || wishlist === void 0 ? void 0 : wishlist.wishlist);
        }
        catch (error) {
            return res.status(500).send({ errorMessage: "Internal error" });
        }
    });
}
exports.getWishlistHandler = getWishlistHandler;
function userCart(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const { cart } = req.body;
        const id = req.params.id;
        try {
            (0, validateMongoDBId_1.default)(id);
            let products = [];
            const user = yield (0, user_service_1.findUserById)(id);
            // check if user already have product in cart
            const alreadyExistCart = yield cart_model_1.default.findOne({ orderby: user === null || user === void 0 ? void 0 : user.id });
            if (alreadyExistCart) {
                yield cart_model_1.default.deleteOne({ _id: alreadyExistCart._id });
            }
            for (let i = 0; i < cart.length; i++) {
                let getPrice = yield (0, audio_service_1.findAudioById)(cart[i].id).select("price").exec();
                let object = {
                    product: cart[i].id,
                    price: getPrice.price,
                };
                products.push(object);
            }
            let cartTotal = 0;
            for (let i = 0; i < products.length; i++) {
                cartTotal = cartTotal + products[i].price;
            }
            let newCart = yield new cart_model_1.default({
                products,
                cartTotal,
                orderby: user === null || user === void 0 ? void 0 : user.id,
            }).save();
            res.status(200).send(newCart);
        }
        catch (error) {
            return res.status(500).send({ errorMessage: "Internal error" });
        }
    });
}
exports.userCart = userCart;
function getUserCart(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const id = req.params.id;
        (0, validateMongoDBId_1.default)(id);
        try {
            const cart = yield cart_model_1.default.findOne({ orderby: id }).populate("products.product");
            res.status(200).send(cart);
        }
        catch (error) {
            return res.status(500).send({ errorMessage: "Internal error" });
        }
    });
}
exports.getUserCart = getUserCart;
function emptyUserCart(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const id = req.params.id;
        (0, validateMongoDBId_1.default)(id);
        try {
            const cart = yield cart_model_1.default.findOneAndRemove({ orderby: id });
            res.status(200).json(cart);
        }
        catch (error) {
            return res.status(500).send(error);
        }
    });
}
exports.emptyUserCart = emptyUserCart;
function createOrder(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const id = req.params.id;
        const { PO } = req.body;
        (0, validateMongoDBId_1.default)(id);
        try {
            if (!PO)
                throw new Error("Create online payment failed");
            let user = yield (0, user_service_1.findUserById)(id);
            let userCart = yield cart_model_1.default.findOne({ orderby: id });
            if (!userCart) {
                return res.json({ message: "Cart is empty" });
            }
            console.log(userCart.products.length);
            if (userCart.products.length === 0) {
                return res.json({ message: "Please insert a product into cart" });
            }
            let finalAmout = userCart === null || userCart === void 0 ? void 0 : userCart.cartTotal;
            // maybe I'll add coupon logic here
            let newOrder = yield new order_model_1.default({
                products: userCart === null || userCart === void 0 ? void 0 : userCart.products,
                paymentIntent: {
                    id: (0, nanoid_1.nanoid)(),
                    method: "PO",
                    amount: finalAmout,
                    status: "Payment Online",
                    created: Date.now(),
                    currency: "usd", // maybe we'll change this to suit the client's country
                },
                orderby: user === null || user === void 0 ? void 0 : user._id,
                orderStatus: "Payment Online",
            }).save();
            res.status(200).json({ message: newOrder });
        }
        catch (error) {
            return res.status(500).send(error);
        }
    });
}
exports.createOrder = createOrder;
function getOrderByUserId(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const id = req.params.id;
        (0, validateMongoDBId_1.default)(id);
        try {
            const userOrder = yield order_model_1.default.findOne({ orderby: id })
                .populate("products.product")
                .populate("orderby")
                .exec();
            if (!userOrder) {
                return res.json({ message: "Theres no order created by this user" });
            }
            res.status(200).send(userOrder);
        }
        catch (error) {
            return res.status(500).send({ errorMessage: "Internal error" });
        }
    });
}
exports.getOrderByUserId = getOrderByUserId;
function getAllOrders(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        try {
            const userOrders = yield (0, order_service_1.findAllOrders)()
                .populate("products.product")
                .populate("orderby")
                .exec();
            if (!userOrders) {
                return res.json({ message: "Theres no orders" });
            }
            res.status(200).send(userOrders);
        }
        catch (error) {
            return res.status(500).send({ errorMessage: "Internal error" });
        }
    });
}
exports.getAllOrders = getAllOrders;
function UpdateOrderStatus(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const { status } = req.body;
        const orderId = req.params.id;
        try {
            const updateOrderStatus = yield order_model_1.default.findByIdAndUpdate(orderId, {
                orderStatus: status,
                paymentIntent: {
                    status: status,
                },
            }, { new: true });
            res.status(200).send(updateOrderStatus);
        }
        catch (error) {
            return res.status(500).send({ errorMessage: "Internal error" });
        }
    });
}
exports.UpdateOrderStatus = UpdateOrderStatus;
// I'll add in the Future Save Adresse
