import { Request, Response, NextFunction } from "express";
import { type } from "os";
import { verifyJwt } from "../db/jwt";

export async function requireUser(req: Request, res: Response, next: NextFunction) {
  const user = res.locals.user;
  if (!user) {
    return res.status(403).send({code: 403, message: "User is not logged In"});
  }

  return next();
}

export async function requireUserAdmin(
  req: Request,
  res: Response,
  next: NextFunction
) {
  const user = res.locals.user;
  if (!user) {
    return res.status(403).send({code: 403, message: "User is not logged In"});
  }

  if (user.role !== "admin") {
    return res.status(401).send({ errorMessage: "Access not Authorized, please contact an Admin" });
  }

  return next()
}
