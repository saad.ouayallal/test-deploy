"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getBucket = exports.connectToServer = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
const dotenv_1 = __importDefault(require("dotenv"));
const logger_1 = __importDefault(require("./logger"));
const mongodb_1 = require("mongodb"); // Import Db from "mongodb"
dotenv_1.default.config();
const connectionString = process.env.ATLAS_URI;
if (!connectionString) {
    throw new Error("No connection string in environment variable ATLAS_URI");
}
const client = new mongodb_1.MongoClient(connectionString);
let bucket; // Initialize bucket as null
const connectToServer = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        yield mongoose_1.default.connect(connectionString);
        logger_1.default.info("Successfully connected to MongoDB.");
        const db = client.db("test"); // Get the database instance
        bucket = new mongodb_1.GridFSBucket(db);
        logger_1.default.info("GridFSBucket initialized.");
    }
    catch (error) {
        logger_1.default.error("Error connecting to database: ", error);
        throw error;
    }
});
exports.connectToServer = connectToServer;
// getDb function returns mongoose connection
const getDb = () => mongoose_1.default.connection;
const getBucket = () => {
    if (!bucket) {
        throw new Error("GridFSBucket is not initialized");
    }
    return bucket;
};
exports.getBucket = getBucket;
exports.default = getDb;
