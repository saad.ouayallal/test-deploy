import mongoose from "mongoose";
import dotenv from "dotenv";
import log from "./logger";
import { GridFSBucket, Db, MongoClient } from "mongodb"; // Import Db from "mongodb"

dotenv.config();

const connectionString = process.env.ATLAS_URI;
if (!connectionString) {
  throw new Error("No connection string in environment variable ATLAS_URI");
}
const client = new MongoClient(connectionString);
let bucket: GridFSBucket; // Initialize bucket as null
export const connectToServer = async () => {
  try {
    await mongoose.connect(connectionString);
    log.info("Successfully connected to MongoDB.");

    const db = client.db("test"); // Get the database instance
    bucket = new GridFSBucket(db);

    log.info("GridFSBucket initialized.");
  } catch (error) {
    log.error("Error connecting to database: ", error);
    throw error;
  }
};

// getDb function returns mongoose connection
const getDb = () => mongoose.connection;

export const getBucket = () => {
  if (!bucket) {
    throw new Error("GridFSBucket is not initialized");
  }
  return bucket;
};

export default getDb;
