test('two plus two is four', () => {
  expect(2 + 2).toBe(4);
});

test('one plus one is two', () => {
  expect(1 + 1).toBe(2);
});