back-end/
┣ config/
┃ ┣ custom-env-variables.ts
┃ ┣ default.ts
┃ ┗ production.ts
┣ src/
┃ ┣ controllers/
┃ ┃ ┣ audio.controller.ts
┃ ┃ ┣ auth.controller.ts
┃ ┃ ┣ cart.controller.ts
┃ ┃ ┣ product.controller.ts
┃ ┃ ┗ user.controller.ts
┃ ┣ db/
┃ ┃ ┣ connection.ts
┃ ┃ ┣ jwt.ts
┃ ┃ ┣ logger.ts
┃ ┃ ┗ mailer.ts
┃ ┣ middleware/
┃ ┃ ┣ auth.ts
┃ ┃ ┣ error.ts
┃ ┃ ┣ requireUser.ts
┃ ┃ ┗ validateResource.ts
┃ ┣ models/
┃ ┃ ┣ cart.model.ts
┃ ┃ ┣ product.model.ts
┃ ┃ ┣ session.model.ts
┃ ┃ ┗ user.model.ts
┃ ┣ routes/
┃ ┃ ┣ user/
┃ ┃ ┃ ┣ auth.routes.ts
┃ ┃ ┃ ┣ index.ts
┃ ┃ ┃ ┗ user.routes.ts
┃ ┃ ┣ audio.routes.ts
┃ ┃ ┣ cart.routes.ts
┃ ┃ ┗ product.routes.ts
┃ ┣ schema/
┃ ┃ ┣ auth.schema.ts
┃ ┃ ┗ user.schema.ts
┃ ┣ service/
┃ ┃ ┣ auth.service.ts
┃ ┃ ┗ user.service.ts
┃ ┣ __tests__/
┃ ┃ ┗ test1.spec.ts
┃ ┗ app.ts
┣ .env
┣ .gitignore
┣ architecture.md
┣ jest.config.ts
┣ package-lock.json
┣ package.json
┣ README.md
┗ tsconfig.json